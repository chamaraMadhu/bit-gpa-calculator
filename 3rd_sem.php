<?php 
include "inc/header.php";
include "inc/navbar.php";
?>

    <div class="container mt-5 bg-light" style="min-height: 700px">
      <div class="row pt-2 pb-1" style="background-color: #FF847C ">
        <div class="col text-center ">
        <h5 class="text-center text-light">3<sup>rd</sup> Semester GPA</h5></div>
      </div>

      <div class="row bg-light p-3" style="border-radius: 2px">
        <div class="col-sm-8 col-md-8">

          <div class="row p-3">
            <h5 class="text-muted mb-3">3<sup>rd</sup> Semester</h5>
            <table>
                <tr>
                    <th width="350px">Subject</th>
                    <th class="text-center" width="100px">Type</th>
                    <th class="text-center" width="160px">Is 1st Sitting?</th>
                    <th class="text-center" width="100px">Grade</th>
                </tr>
                <tr>
                    <td>11. Object Oriented Analysis & Design</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_11" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_11" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_11">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>12. Fundamentals of Software Engineering</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_12" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_12" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_12">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>13. Mathematics for Computing II</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_13" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_13" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_13">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>14. User Interface Design</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_14" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_14" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_14">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>15. Web Application Development II</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_15" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_15" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_15">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><button class="btn btn-success btn-sm mt-2" onClick="gpaCal_3();" style="width:100%">Calculate</button></td>
                </tr>
            </table>
          </div>

          <div class="row text-center" id="result_topic" >
            <div class="col">
            <h6 class="text-light pt-2 pb-1" id="heading"></h6>
            </div>
          </div>
          <div class="row pb-3" id="table">
            <div class="col">
              <table class="mt-3" align="center">
                <tr style="font-weight: 700">
                  <td id="subject" width="350"></td>
                  <td id="type" width="100"></td>
                  <td id="credit" width="160"></td>
                  <td id="grade" width="100"></td>
                </tr>
                <tr>
                  <td id="subject_11"></td>
                  <td id="type_11"></td>
                  <td id="credit_11"></td>
                  <td id="grade_11"></td>
                </tr>
                <tr>
                  <td id="subject_12"></td>
                  <td id="type_12"></td>
                  <td id="credit_12"></td>
                  <td id="grade_12"></td>
                </tr>
                <tr>
                  <td id="subject_13"></td>
                  <td id="type_13"></td>
                  <td id="credit_13"></td>
                  <td id="grade_13"></td>
                </tr>
                <tr>
                  <td id="subject_14"></td>
                  <td id="type_14"></td>
                  <td id="credit_14"></td>
                  <td id="grade_14"></td>
                </tr>
                <tr>
                  <td id="subject_15"></td>
                  <td id="type_15"></td>
                  <td id="credit_15"></td>
                  <td id="grade_15"></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td id="class_gpa" style="font-size: 14px; font-weight: 500"></td>
                  <td id="sem_3_gpa" style="font-size: 14px; font-weight: 500"></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td id="gpa_pect" style="font-size: 14px; font-weight: 500"></td>
                  <td id="gpa_percentage" style="font-size: 14px; font-weight: 500"></td>
                </tr>
              </table>
            </div>
          </div>

        </div>

        <div class="col-sm-4 col-md-4 Grading_scheme p-3">
          <h5 class="text-muted text-center mb-3">Grading Scheme</h5>
          <table align="center" border="1">
              <tr>
                  <th class="text-center" width="75px">Marks</th>
                  <th class="text-center" width="80px">Grade</th>
                  <th class="text-center" width="100px">Point Value</th>
              </tr>
              <tr>
                <td>90-100</td>
                <td>A+</td>
                <td>4.00</td>
              </tr>
              <tr>
                <td>80-89</td>
                <td>A</td>
                <td>4.00</td>
              </tr>
              <tr>
                <td>75-79</td>
                <td>A-</td>
                <td>3.67</td>
              </tr>
              <tr>
                <td>70-74</td>
                <td>B+</td>
                <td>3.33</td>
              </tr>
              <tr>
                <td>65-69</td>
                <td>B</td>
                <td>3.00</td>
              </tr>
              <tr>
                <td>60-64</td>
                <td>B-</td>
                <td>2.67</td>
              </tr>
              <tr>
                <td>55-59</td>
                <td>C+</td>
                <td>2.33</td>
              </tr>
              <tr>
                <td>50-54</td>
                <td>C</td>
                <td>2.00</td>
              </tr>
              <tr>
                <td>45-49</td>
                <td>C-</td>
                <td>1.67</td>
              </tr>
              <tr>
                <td>40-44</td>
                <td>D+</td>
                <td>1.33</td>
              </tr>
              <tr>
                <td>30-39</td>
                <td>D</td>
                <td>1.00</td>
              </tr>
              <tr>
                <td>20-29</td>
                <td>D-</td>
                <td>0.67</td>
              </tr>
              <tr>
                <td>0-19</td>
                <td>E</td>
                <td>0.00</td>
              </tr>
              <tr>
                <td>-</td>
                <td style="padding-left: 0px; text-align: center">Not Sat</td>
                <td>0.00</td>
              </tr>
          </table>
          <div style="font-size: 14px">
            <p class="text-center mt-3"><b class="text-danger">**</b> Not Sat = You don't Still face the subject or skipped</p>
            <p class="text-center mt-0"><b class="text-danger">**</b> Non-GPA = Not Applicable for GPA calculation</p>
          </div>
        </div>

      </div>
    </div>

    <script type="text/javascript">
      function gpaCal_3(){

        var grade_11 = document.getElementById('sub_11').value;
        var grade_12 = document.getElementById('sub_12').value;
        var grade_13 = document.getElementById('sub_13').value;
        var grade_14 = document.getElementById('sub_14').value;
        var grade_15 = document.getElementById('sub_15').value;

        var sub_11;
        var sub_12;
        var sub_13;
        var sub_14;
        var sub_15;

        if(document.getElementsByName('sitting_sub_11')[0].checked){

            switch(grade_11){
              case 'Not Sat':
                sub_11 = 0;
                break;
              case 'A+':
                sub_11 = 4;
                break;
              case 'A':
                sub_11 = 4;
                break;
              case 'A-':
                sub_11 = 3.67;
                break;
              case 'B+':
                sub_11 = 3.33;
                break;
              case 'B':
                sub_11 = 3;
                break;
              case 'B-':
                sub_11 = 2.67;
                break;
              case 'C+':
                sub_11 = 2.33;
                break;
              case 'C':
                sub_11 = 2;
                break;
              case 'C-':
                sub_11 = 1.67;
                break;
              case 'D+':
                sub_11 = 1.33;
                break;
              case 'D':
                sub_11 = 1;
                break;
              case 'D-':
                sub_11 = .67;
                break;
              case 'E':
                sub_11 = 0;
                break;
            }

        }else{
          switch(grade_11){
              case 'C-':
                sub_11 = 1.67;
                break;
              case 'D+':
                sub_11 = 1.33;
                break;
              case 'D':
                sub_11 = 1;
                break;
              case 'D-':
                sub_11 = .67;
                break;
              case 'E':
                sub_11 = 0;
                break;
              default :
                sub_11 = 2;
            }
          
        }

        if(document.getElementsByName('sitting_sub_12')[0].checked){
          
            switch(grade_12){
              case 'Not Sat':
                sub_12 = 0;
                break;
              case 'A+':
                sub_12 = 4;
                break;
              case 'A':
                sub_12 = 4;
                break;
              case 'A-':
                sub_12 = 3.67;
                break;
              case 'B+':
                sub_12 = 3.33;
                break;
              case 'B':
                sub_12 = 3;
                break;
              case 'B-':
                sub_12 = 2.67;
                break;
              case 'C+':
                sub_12 = 2.33;
                break;
              case 'C':
                sub_12 = 2;
                break;
              case 'C-':
                sub_12 = 1.67;
                break;
              case 'D+':
                sub_12 = 1.33;
                break;
              case 'D':
                sub_12 = 1;
                break;
              case 'D-':
                sub_12 = .67;
                break;
              case 'E':
                sub_12 = 0;
                break;
            }

        }else{
          switch(grade_12){
              case 'C-':
                sub_12 = 1.67;
                break;
              case 'D+':
                sub_12 = 1.33;
                break;
              case 'D':
                sub_12 = 1;
                break;
              case 'D-':
                sub_12 = .67;
                break;
              case 'E':
                sub_12 = 0;
                break;
              default :
                sub_12 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_13')[0].checked){
          
            switch(grade_13){
              case 'Not Sat':
                sub_13 = 0;
                break;
              case 'A+':
                sub_13 = 4;
                break;
              case 'A':
                sub_13 = 4;
                break;
              case 'A-':
                sub_13 = 3.67;
                break;
              case 'B+':
                sub_13 = 3.33;
                break;
              case 'B':
                sub_13 = 3;
                break;
              case 'B-':
                sub_13 = 2.67;
                break;
              case 'C+':
                sub_13 = 2.33;
                break;
              case 'C':
                sub_13 = 2;
                break;
              case 'C-':
                sub_13 = 1.67;
                break;
              case 'D+':
                sub_13 = 1.33;
                break;
              case 'D':
                sub_13 = 1;
                break;
              case 'D-':
                sub_13 = .67;
                break;
              case 'E':
                sub_13 = 0;
                break;
            }

        }else{
          switch(grade_13){
              case 'C-':
                sub_13 = 1.67;
                break;
              case 'D+':
                sub_13 = 1.33;
                break;
              case 'D':
                sub_13 = 1;
                break;
              case 'D-':
                sub_13 = .67;
                break;
              case 'E':
                sub_13 = 0;
                break;
              default :
                sub_13 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_14')[0].checked){
          
            switch(grade_14){
              case 'Not Sat':
                sub_14 = 0;
                break;
              case 'A+':
                sub_14 = 4;
                break;
              case 'A':
                sub_14 = 4;
                break;
              case 'A-':
                sub_14 = 3.67;
                break;
              case 'B+':
                sub_14 = 3.33;
                break;
              case 'B':
                sub_14 = 3;
                break;
              case 'B-':
                sub_14 = 2.67;
                break;
              case 'C+':
                sub_14 = 2.33;
                break;
              case 'C':
                sub_14 = 2;
                break;
              case 'C-':
                sub_14 = 1.67;
                break;
              case 'D+':
                sub_14 = 1.33;
                break;
              case 'D':
                sub_14 = 1;
                break;
              case 'D-':
                sub_14 = .67;
                break;
              case 'E':
                sub_14 = 0;
                break;
            }

        }else{
          switch(grade_14){
              case 'C-':
                sub_14 = 1.67;
                break;
              case 'D+':
                sub_14 = 1.33;
                break;
              case 'D':
                sub_14 = 1;
                break;
              case 'D-':
                sub_14 = .67;
                break;
              case 'E':
                sub_14 = 0;
                break;
              default :
                sub_14= 2;
            }
        }

        if(document.getElementsByName('sitting_sub_15')[0].checked){
          
            switch(grade_15){
              case 'Not Sat':
                sub_15 = 0;
                break;
              case 'A+':
                sub_15 = 4;
                break;
              case 'A':
                sub_15 = 4;
                break;
              case 'A-':
                sub_15 = 3.67;
                break;
              case 'B+':
                sub_15 = 3.33;
                break;
              case 'B':
                sub_15 = 3;
                break;
              case 'B-':
                sub_15 = 2.67;
                break;
              case 'C+':
                sub_15 = 2.33;
                break;
              case 'C':
                sub_15 = 2;
                break;
              case 'C-':
                sub_15 = 1.67;
                break;
              case 'D+':
                sub_15 = 1.33;
                break;
              case 'D':
                sub_15 = 1;
                break;
              case 'D-':
                sub_15 = .67;
                break;
              case 'E':
                sub_15 = 0;
                break;
            }

        }else{
          switch(grade_15){
              case 'C-':
                sub_15 = 1.67;
                break;
              case 'D+':
                sub_15 = 1.33;
                break;
              case 'D':
                sub_15 = 1;
                break;
              case 'D-':
                sub_15 = .67;
                break;
              case 'E':
                sub_15 = 0;
                break;
              default :
                sub_15 = 2;
            }
        }

        var gpa = ((sub_11*3)+(sub_12*3)+(sub_13*3)+(sub_14*3)+(sub_15*4))/16;
        document.getElementById("sem_3_gpa").innerHTML = gpa.toFixed(2);

        var gpa_percentage = (gpa/4)*100;
        document.getElementById("gpa_percentage").innerHTML = gpa_percentage.toFixed(2)+"%";

        document.getElementById("heading").innerHTML = "Result Sheet";
        document.getElementById("subject_11").innerHTML = "11. Object Oriented Analysis & Design";
        document.getElementById("subject_12").innerHTML = "12. Fundamentals of Software Engineering";
        document.getElementById("subject_13").innerHTML = "13. Mathematics for Computing II";
        document.getElementById("subject_14").innerHTML = "14. User Interface Design";
        document.getElementById("subject_15").innerHTML = "15. Web Application Development II";

        document.getElementById("type_11").innerHTML = "GPA";
        document.getElementById("type_12").innerHTML = "GPA";
        document.getElementById("type_13").innerHTML = "GPA";
        document.getElementById("type_14").innerHTML = "GPA";
        document.getElementById("type_15").innerHTML = "GPA";

        document.getElementById("credit_11").innerHTML = "3";
        document.getElementById("credit_12").innerHTML = "3";
        document.getElementById("credit_13").innerHTML = "3";
        document.getElementById("credit_14").innerHTML = "3";
        document.getElementById("credit_15").innerHTML = "4";

        document.getElementById("grade_11").innerHTML = grade_11;
        document.getElementById("grade_12").innerHTML = grade_12;
        document.getElementById("grade_13").innerHTML = grade_13;
        document.getElementById("grade_14").innerHTML = grade_14;
        document.getElementById("grade_15").innerHTML = grade_15;

        document.getElementById("type").innerHTML = "Type";
        document.getElementById("subject").innerHTML = "Subject";
        document.getElementById("credit").innerHTML = "Credit";
        document.getElementById("grade").innerHTML = "Grade";

        document.getElementById("class_gpa").innerHTML = "Class GPA";
        document.getElementById("gpa_pect").innerHTML = "% of GPA";

        document.getElementById("table").style.border ="1px solid lightgray";
        document.getElementById("sem_3_gpa").style.background ="lightgreen";
        document.getElementById("gpa_percentage").style.background ="yellow";
        document.getElementById("table").style.background ="#fff";
        document.getElementById("result_topic").style.background ="#A8A7A7";

      }
    </script>

<?php 
include "inc/footer.php";
?>