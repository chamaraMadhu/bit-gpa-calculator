<?php 
include "inc/header.php";
include "inc/navbar.php";
?>

    <div class="container mt-5 bg-light" style="min-height: 700px">
      <div class="row pt-2 pb-1" style="background-color: #FF847C ">
        <div class="col text-center ">
        <h5 class="text-center text-light">2<sup>nd</sup> Semester GPA</h5></div>
      </div>

      <div class="row bg-light p-3" style="border-radius: 2px">
        <div class="col-sm-8 col-md-8">

          <div class="row p-3">
            <div><h5 class="text-muted mb-3">2<sup>nd</sup> Semester</h5></div>
            <table>
                <tr>
                    <th width="350px">Subject</th>
                    <th class="text-center" width="100px">Type</th>
                    <th class="text-center" width="160px">Is 1st Sitting?</th>
                    <th class="text-center" width="100px">Grade</th>
                </tr>
                <tr>
                    <td>7. Mathematics for Computing I</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_7" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_7" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_7">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>8. Programming I</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_8" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_8" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_8">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>9. Database Systems I</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_9" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_9" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_9">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>10. Systems Analysis & Design</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_10" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_10" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_10">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><button class="btn btn-success btn-sm mt-2" onClick="gpaCal_2();" style="width:100%">Calculate</button></td>
                </tr>
            </table>
          </div>

          <div class="row text-center" id="result_topic" >
            <div class="col">
            <h6 class="text-light pt-2 pb-1" id="heading"></h6>
            </div>
          </div>
          <div class="row pb-3" id="table">
            <div class="col p-4 ">
              <table class="mt-3" align="center">
                <tr style="font-weight: 700">
                  <td id="subject" width="350"></td>
                  <td id="type" width="100"></td>
                  <td id="credit" width="160"></td>
                  <td id="grade" width="100"></td>
                </tr>
                <tr>
                  <td id="subject_7"></td>
                  <td id="type_7"></td>
                  <td id="credit_7"></td>
                  <td id="grade_7"></td>
                </tr>
                <tr>
                  <td id="subject_8"></td>
                  <td id="type_8"></td>
                  <td id="credit_8"></td>
                  <td id="grade_8"></td>
                </tr>
                <tr>
                  <td id="subject_9"></td>
                  <td id="type_9"></td>
                  <td id="credit_9"></td>
                  <td id="grade_9"></td>
                </tr>
                <tr>
                  <td id="subject_10"></td>
                  <td id="type_10"></td>
                  <td id="credit_10"></td>
                  <td id="grade_10"></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td id="class_gpa" style="font-size: 14px; font-weight: 500"></td>
                  <td id="sem_2_gpa" style="font-size: 14px; font-weight: 500"></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td id="gpa_pect" style="font-size: 14px; font-weight: 500"></td>
                  <td id="gpa_percentage" style="font-size: 14px; font-weight: 500"></td>
                </tr>
              </table>
            </div>
          </div>

        </div>

        <div class="col-sm-4 col-md-4 Grading_scheme p-3">
          <h5 class="text-muted text-center mb-3">Grading Scheme</h5>
          <table align="center" border="1">
              <tr>
                  <th class="text-center" width="75px">Marks</th>
                  <th class="text-center" width="80px">Grade</th>
                  <th class="text-center" width="100px">Point Value</th>
              </tr>
              <tr>
                <td>90-100</td>
                <td>A+</td>
                <td>4.00</td>
              </tr>
              <tr>
                <td>80-89</td>
                <td>A</td>
                <td>4.00</td>
              </tr>
              <tr>
                <td>75-79</td>
                <td>A-</td>
                <td>3.67</td>
              </tr>
              <tr>
                <td>70-74</td>
                <td>B+</td>
                <td>3.33</td>
              </tr>
              <tr>
                <td>65-69</td>
                <td>B</td>
                <td>3.00</td>
              </tr>
              <tr>
                <td>60-64</td>
                <td>B-</td>
                <td>2.67</td>
              </tr>
              <tr>
                <td>55-59</td>
                <td>C+</td>
                <td>2.33</td>
              </tr>
              <tr>
                <td>50-54</td>
                <td>C</td>
                <td>2.00</td>
              </tr>
              <tr>
                <td>45-49</td>
                <td>C-</td>
                <td>1.67</td>
              </tr>
              <tr>
                <td>40-44</td>
                <td>D+</td>
                <td>1.33</td>
              </tr>
              <tr>
                <td>30-39</td>
                <td>D</td>
                <td>1.00</td>
              </tr>
              <tr>
                <td>20-29</td>
                <td>D-</td>
                <td>0.67</td>
              </tr>
              <tr>
                <td>0-19</td>
                <td>E</td>
                <td>0.00</td>
              </tr>
              <tr>
                <td>-</td>
                <td style="padding-left: 0px; text-align: center">Not Sat</td>
                <td>0.00</td>
              </tr>
          </table>
          <div style="font-size: 14px">
            <p class="text-center mt-3"><b class="text-danger">**</b> Not Sat = You don't Still face the subject or skipped</p>
            <p class="text-center mt-0"><b class="text-danger">**</b> Non-GPA = Not Applicable for GPA calculation</p>
          </div>
        </div>

      </div>
    </div>

    <script type="text/javascript">
      function gpaCal_2(){

        var grade_7 = document.getElementById('sub_7').value;
        var grade_8 = document.getElementById('sub_8').value;
        var grade_9 = document.getElementById('sub_9').value;
        var grade_10 = document.getElementById('sub_10').value;

        var sub_7;
        var sub_8;
        var sub_9;
        var sub_10;

        if(document.getElementsByName('sitting_sub_7')[0].checked){

            switch(grade_7){
              case 'Not Sat':
                sub_7 = 0;
                break;
              case 'A+':
                sub_7 = 4;
                break;
              case 'A':
                sub_7 = 4;
                break;
              case 'A-':
                sub_7 = 3.67;
                break;
              case 'B+':
                sub_7 = 3.33;
                break;
              case 'B':
                sub_7 = 3;
                break;
              case 'B-':
                sub_7 = 2.67;
                break;
              case 'C+':
                sub_7 = 2.33;
                break;
              case 'C':
                sub_7 = 2;
                break;
              case 'C-':
                sub_7 = 1.67;
                break;
              case 'D+':
                sub_7 = 1.33;
                break;
              case 'D':
                sub_7 = 1;
                break;
              case 'D-':
                sub_7 = .67;
                break;
              case 'E':
                sub_7 = 0;
                break;
            }

        }else{
          switch(grade_7){
              case 'C-':
                sub_7 = 1.67;
                break;
              case 'D+':
                sub_7 = 1.33;
                break;
              case 'D':
                sub_7 = 1;
                break;
              case 'D-':
                sub_7 = .67;
                break;
              case 'E':
                sub_7 = 0;
                break;
              default :
                sub_7 = 2;
            }
          
        }

        if(document.getElementsByName('sitting_sub_8')[0].checked){
          
            switch(grade_8){
              case 'Not Sat':
                sub_8 = 0;
                break;
              case 'A+':
                sub_8 = 4;
                break;
              case 'A':
                sub_8 = 4;
                break;
              case 'A-':
                sub_8 = 3.67;
                break;
              case 'B+':
                sub_8 = 3.33;
                break;
              case 'B':
                sub_8 = 3;
                break;
              case 'B-':
                sub_8 = 2.67;
                break;
              case 'C+':
                sub_8 = 2.33;
                break;
              case 'C':
                sub_8 = 2;
                break;
              case 'C-':
                sub_8 = 1.67;
                break;
              case 'D+':
                sub_8 = 1.33;
                break;
              case 'D':
                sub_8 = 1;
                break;
              case 'D-':
                sub_8 = .67;
                break;
              case 'E':
                sub_8 = 0;
                break;
            }

        }else{
          switch(grade_8){
              case 'C-':
                sub_8 = 1.67;
                break;
              case 'D+':
                sub_8 = 1.33;
                break;
              case 'D':
                sub_8 = 1;
                break;
              case 'D-':
                sub_8 = .67;
                break;
              case 'E':
                sub_8 = 0;
                break;
              default :
                sub_8 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_9')[0].checked){
          
            switch(grade_9){
              case 'Not Sat':
                sub_9 = 0;
                break;
              case 'A+':
                sub_9 = 4;
                break;
              case 'A':
                sub_9 = 4;
                break;
              case 'A-':
                sub_9 = 3.67;
                break;
              case 'B+':
                sub_9 = 3.33;
                break;
              case 'B':
                sub_9 = 3;
                break;
              case 'B-':
                sub_9 = 2.67;
                break;
              case 'C+':
                sub_9 = 2.33;
                break;
              case 'C':
                sub_9 = 2;
                break;
              case 'C-':
                sub_9 = 1.67;
                break;
              case 'D+':
                sub_9 = 1.33;
                break;
              case 'D':
                sub_9 = 1;
                break;
              case 'D-':
                sub_9 = .67;
                break;
              case 'E':
                sub_9 = 0;
                break;
            }

        }else{
          switch(grade_9){
              case 'C-':
                sub_9 = 1.67;
                break;
              case 'D+':
                sub_9 = 1.33;
                break;
              case 'D':
                sub_9 = 1;
                break;
              case 'D-':
                sub_9 = .67;
                break;
              case 'E':
                sub_9 = 0;
                break;
              default :
                sub_9 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_10')[0].checked){
            switch(grade_10){
              case 'Not Sat':
                sub_10 = 0;
                break;
              case 'A+':
                sub_10 = 4;
                break;
              case 'A':
                sub_10 = 4;
                break;
              case 'A-':
                sub_10 = 3.67;
                break;
              case 'B+':
                sub_10 = 3.33;
                break;
              case 'B':
                sub_10 = 3;
                break;
              case 'B-':
                sub_10 = 2.67;
                break;
              case 'C+':
                sub_10 = 2.33;
                break;
              case 'C':
                sub_10 = 2;
                break;
              case 'C-':
                sub_10 = 1.67;
                break;
              case 'D+':
                sub_10 = 1.33;
                break;
              case 'D':
                sub_10= 1;
                break;
              case 'D-':
                sub_10 = .67;
                break;
              case 'E':
                sub_10 = 0;
                break;
            }

        }else{
          switch(grade_10){
              case 'C-':
                sub_10 = 1.67;
                break;
              case 'D+':
                sub_10 = 1.33;
                break;
              case 'D':
                sub_10 = 1;
                break;
              case 'D-':
                sub_10 = .67;
                break;
              case 'E':
                sub_10 = 0;
                break;
              default :
                sub_10 = 2;
            }
        }

        var gpa = ((sub_7*3)+(sub_8*4)+(sub_9*4)+(sub_10*3))/14;
        document.getElementById("sem_2_gpa").innerHTML = gpa.toFixed(2);

        var gpa_percentage = (gpa/4)*100;
        document.getElementById("gpa_percentage").innerHTML = gpa_percentage.toFixed(2)+"%";

        document.getElementById("heading").innerHTML = "Result Sheet";
        document.getElementById("subject_7").innerHTML = "7. Mathematics for Computing I";
        document.getElementById("subject_8").innerHTML = "8. Programming I";
        document.getElementById("subject_9").innerHTML = "9. Database Systems I";
        document.getElementById("subject_10").innerHTML = "10. Systems Analysis & Design";

        document.getElementById("type_7").innerHTML = "GPA";
        document.getElementById("type_8").innerHTML = "GPA";
        document.getElementById("type_9").innerHTML = "GPA";
        document.getElementById("type_10").innerHTML = "GPA";

        document.getElementById("credit_7").innerHTML = "3";
        document.getElementById("credit_8").innerHTML = "4";
        document.getElementById("credit_9").innerHTML = "4";
        document.getElementById("credit_10").innerHTML = "3";

        document.getElementById("grade_7").innerHTML = grade_7;
        document.getElementById("grade_8").innerHTML = grade_8;
        document.getElementById("grade_9").innerHTML = grade_9;
        document.getElementById("grade_10").innerHTML = grade_10;

        document.getElementById("type").innerHTML = "Type";
        document.getElementById("subject").innerHTML = "Subject";
        document.getElementById("credit").innerHTML = "Credit";
        document.getElementById("grade").innerHTML = "Grade";

        document.getElementById("class_gpa").innerHTML = "Class GPA";
        document.getElementById("gpa_pect").innerHTML = "% of GPA";

        document.getElementById("table").style.border ="1px solid lightgray";
        document.getElementById("sem_2_gpa").style.background ="lightgreen";
        document.getElementById("gpa_percentage").style.background ="yellow";
        document.getElementById("table").style.background ="#fff";
        document.getElementById("result_topic").style.background ="#A8A7A7";  
      }
    </script>

<?php 
include "inc/footer.php";
?>