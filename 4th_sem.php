<?php 
include "inc/header.php";
include "inc/navbar.php";
?>

  <div class="container mt-5 bg-light" style="min-height: 700px">
      <div class="row pt-2 pb-1" style="background-color: #FF847C ">
        <div class="col text-center ">
        <h5 class="text-center text-light">4<sup>th</sup> Semester GPA</h5></div>
      </div>

      <div class="row bg-white p-3" style="border-radius: 2px">
        <div class="col-sm-8 col-md-8">

          <div class="row p-3">
            <h5 class="text-muted mb-3">4<sup>th</sup> Semester</h5>
            <table>
                <tr>
                    <th width="350px">Subject</th>
                    <th class="text-center" width="100px">Type</th>
                    <th class="text-center" width="160px">Is 1st Sitting?</th>
                    <th class="text-center" width="100px">Grade</th>
                </tr>
                <tr>
                    <td>16. Programming II</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_16" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_16" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_16">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>17. Information Technology Project Mgt.</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_17" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_17" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_17">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>18. Rapid Software Development</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_18" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_18" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_18">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>19. Computer Networks</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_19" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_19" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_19">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><button class="btn btn-success btn-sm mt-2" onClick="gpaCal_4();" style="width:100%">Calculate</button></td>
                </tr>
            </table>
          </div>

          <div class="row text-center" id="result_topic" >
            <div class="col">
            <h6 class="text-light pt-2 pb-1" id="heading"></h6>
            </div>
          </div>
          <div class="row pb-3" id="table">
            <div class="col">
              <table class="mt-3" align="center">
                <tr style="font-weight: 700">
                  <td id="subject" width="350"></td>
                  <td id="type" width="100"></td>
                  <td id="credit" width="160"></td>
                  <td id="grade" width="100"></td>
                </tr>
                <tr>
                  <td id="subject_16"></td>
                  <td id="type_16"></td>
                  <td id="credit_16"></td>
                  <td id="grade_16"></td>
                </tr>
                <tr>
                  <td id="subject_17"></td>
                  <td id="type_17"></td>
                  <td id="credit_17"></td>
                  <td id="grade_17"></td>
                </tr>
                <tr>
                  <td id="subject_18"></td>
                  <td id="type_18"></td>
                  <td id="credit_18"></td>
                  <td id="grade_18"></td>
                </tr>
                <tr>
                  <td id="subject_19"></td>
                  <td id="type_19"></td>
                  <td id="credit_19"></td>
                  <td id="grade_19"></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td id="class_gpa" style="font-size: 19px; font-weight: 500"></td>
                  <td id="sem_4_gpa" style="font-size: 19px; font-weight: 500"></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td id="gpa_pect" style="font-size: 19px; font-weight: 500"></td>
                  <td id="gpa_percentage" style="font-size: 19px; font-weight: 500"></td>
                </tr>
              </table>
            </div>
          </div>

        </div>

        <div class="col-sm-4 col-md-4 Grading_scheme p-3">
          <h5 class="text-muted text-center mb-3">Grading Scheme</h5>
          <table align="center" border="1">
              <tr>
                  <th class="text-center" width="75px">Marks</th>
                  <th class="text-center" width="80px">Grade</th>
                  <th class="text-center" width="100px">Point Value</th>
              </tr>
              <tr>
                <td>90-100</td>
                <td>A+</td>
                <td>4.00</td>
              </tr>
              <tr>
                <td>80-89</td>
                <td>A</td>
                <td>4.00</td>
              </tr>
              <tr>
                <td>75-79</td>
                <td>A-</td>
                <td>3.67</td>
              </tr>
              <tr>
                <td>70-74</td>
                <td>B+</td>
                <td>3.33</td>
              </tr>
              <tr>
                <td>65-69</td>
                <td>B</td>
                <td>3.00</td>
              </tr>
              <tr>
                <td>60-64</td>
                <td>B-</td>
                <td>2.67</td>
              </tr>
              <tr>
                <td>55-59</td>
                <td>C+</td>
                <td>2.33</td>
              </tr>
              <tr>
                <td>50-54</td>
                <td>C</td>
                <td>2.00</td>
              </tr>
              <tr>
                <td>45-49</td>
                <td>C-</td>
                <td>1.67</td>
              </tr>
              <tr>
                <td>40-44</td>
                <td>D+</td>
                <td>1.33</td>
              </tr>
              <tr>
                <td>30-39</td>
                <td>D</td>
                <td>1.00</td>
              </tr>
              <tr>
                <td>20-29</td>
                <td>D-</td>
                <td>0.67</td>
              </tr>
              <tr>
                <td>0-19</td>
                <td>E</td>
                <td>0.00</td>
              </tr>
              <tr>
                <td>-</td>
                <td style="padding-left: 0px; text-align: center">Not Sat</td>
                <td>0.00</td>
              </tr>
          </table>
          <div style="font-size: 14px">
            <p class="text-center mt-3"><b class="text-danger">**</b> Not Sat = You don't Still face the subject or skipped</p>
            <p class="text-center mt-0"><b class="text-danger">**</b> Non-GPA = Not Applicable for GPA calculation</p>
          </div>
        </div>

      </div>
    </div>

    <script type="text/javascript">

    function gpaCal_4(){

        var grade_16 = document.getElementById('sub_16').value;
        var grade_17 = document.getElementById('sub_17').value;
        var grade_18 = document.getElementById('sub_18').value;
        var grade_19 = document.getElementById('sub_19').value;

        var sub_16;
        var sub_17;
        var sub_18;
        var sub_19;

        if(document.getElementsByName('sitting_sub_16')[0].checked){

            switch(grade_16){
              case 'Not Sat':
                sub_16 = 0;
                break;
              case 'A+':
                sub_16 = 4;
                break;
              case 'A':
                sub_16 = 4;
                break;
              case 'A-':
                sub_16 = 3.67;
                break;
              case 'B+':
                sub_16 = 3.33;
                break;
              case 'B':
                sub_16 = 3;
                break;
              case 'B-':
                sub_16 = 2.67;
                break;
              case 'C+':
                sub_16 = 2.33;
                break;
              case 'C':
                sub_16 = 2;
                break;
              case 'C-':
                sub_16 = 1.67;
                break;
              case 'D+':
                sub_16= 1.33;
                break;
              case 'D':
                sub_16 = 1;
                break;
              case 'D-':
                sub_16 = .67;
                break;
              case 'E':
                sub_16 = 0;
                break;
            }

        }else{
          switch(grade_16){
              case 'C-':
                sub_16 = 1.67;
                break;
              case 'D+':
                sub_16 = 1.33;
                break;
              case 'D':
                sub_16 = 1;
                break;
              case 'D-':
                sub_16 = .67;
                break;
              case 'E':
                sub_16 = 0;
                break;
              default :
                sub_16 = 2;
            }
          
        }

        if(document.getElementsByName('sitting_sub_17')[0].checked){
          
            switch(grade_17){
              case 'Not Sat':
                sub_17 = 0;
                break;
              case 'A+':
                sub_17 = 4;
                break;
              case 'A':
                sub_17 = 4;
                break;
              case 'A-':
                sub_17 = 3.67;
                break;
              case 'B+':
                sub_17 = 3.33;
                break;
              case 'B':
                sub_17 = 3;
                break;
              case 'B-':
                sub_17 = 2.67;
                break;
              case 'C+':
                sub_17 = 2.33;
                break;
              case 'C':
                sub_17 = 2;
                break;
              case 'C-':
                sub_17 = 1.67;
                break;
              case 'D+':
                sub_17 = 1.33;
                break;
              case 'D':
                sub_17 = 1;
                break;
              case 'D-':
                sub_17 = .67;
                break;
              case 'E':
                sub_17 = 0;
                break;
            }

        }else{
          switch(grade_17){
              case 'C-':
                sub_17 = 1.67;
                break;
              case 'D+':
                sub_17 = 1.33;
                break;
              case 'D':
                sub_17 = 1;
                break;
              case 'D-':
                sub_17 = .67;
                break;
              case 'E':
                sub_17 = 0;
                break;
              default :
                sub_17 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_18')[0].checked){
          
            switch(grade_18){
              case 'Not Sat':
                sub_18 = 0;
                break;
              case 'A+':
                sub_18 = 4;
                break;
              case 'A':
                sub_18 = 4;
                break;
              case 'A-':
                sub_18 = 3.67;
                break;
              case 'B+':
                sub_18 = 3.33;
                break;
              case 'B':
                sub_18 = 3;
                break;
              case 'B-':
                sub_18 = 2.67;
                break;
              case 'C+':
                sub_18 = 2.33;
                break;
              case 'C':
                sub_18 = 2;
                break;
              case 'C-':
                sub_18 = 1.67;
                break;
              case 'D+':
                sub_18 = 1.33;
                break;
              case 'D':
                sub_18 = 1;
                break;
              case 'D-':
                sub_18 = .67;
                break;
              case 'E':
                sub_18 = 0;
                break;
            }

        }else{
          switch(grade_18){
              case 'C-':
                sub_18 = 1.67;
                break;
              case 'D+':
                sub_18 = 1.33;
                break;
              case 'D':
                sub_18 = 1;
                break;
              case 'D-':
                sub_18 = .67;
                break;
              case 'E':
                sub_18 = 0;
                break;
              default :
                sub_18 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_19')[0].checked){
          
            switch(grade_19){
              case 'Not Sat':
                sub_19 = 0;
                break;
              case 'A+':
                sub_19 = 4;
                break;
              case 'A':
                sub_19 = 4;
                break;
              case 'A-':
                sub_19 = 3.67;
                break;
              case 'B+':
                sub_19 = 3.33;
                break;
              case 'B':
                sub_19 = 3;
                break;
              case 'B-':
                sub_19 = 2.67;
                break;
              case 'C+':
                sub_19 = 2.33;
                break;
              case 'C':
                sub_19 = 2;
                break;
              case 'C-':
                sub_19 = 1.67;
                break;
              case 'D+':
                sub_19 = 1.33;
                break;
              case 'D':
                sub_19 = 1;
                break;
              case 'D-':
                sub_19 = .67;
                break;
              case 'E':
                sub_19 = 0;
                break;
            }

        }else{
          switch(grade_19){
              case 'C-':
                sub_19 = 1.67;
                break;
              case 'D+':
                sub_19 = 1.33;
                break;
              case 'D':
                sub_19 = 1;
                break;
              case 'D-':
                sub_19 = .67;
                break;
              case 'E':
                sub_19 = 0;
                break;
              default :
                sub_19= 2;
            }
        }

        var gpa = ((sub_16*4)+(sub_17*3)+(sub_18*4)+(sub_19*3))/14;
        document.getElementById("sem_4_gpa").innerHTML = gpa.toFixed(2);

        var gpa_percentage = (gpa/4)*100;
        document.getElementById("gpa_percentage").innerHTML = gpa_percentage.toFixed(2)+"%";

        document.getElementById("heading").innerHTML = "Result Sheet";
        document.getElementById("subject_16").innerHTML = "16. Programming II";
        document.getElementById("subject_17").innerHTML = "17. Information Technology Project Mgt.";
        document.getElementById("subject_18").innerHTML = "18. Rapid Software Development";
        document.getElementById("subject_19").innerHTML = "19. Computer Networks";

        document.getElementById("type_16").innerHTML = "GPA";
        document.getElementById("type_17").innerHTML = "GPA";
        document.getElementById("type_18").innerHTML = "GPA";
        document.getElementById("type_19").innerHTML = "GPA";

        document.getElementById("credit_16").innerHTML = "4";
        document.getElementById("credit_17").innerHTML = "3";
        document.getElementById("credit_18").innerHTML = "4";
        document.getElementById("credit_19").innerHTML = "3";

        document.getElementById("grade_16").innerHTML = grade_16;
        document.getElementById("grade_17").innerHTML = grade_17;
        document.getElementById("grade_18").innerHTML = grade_18;
        document.getElementById("grade_19").innerHTML = grade_19;

        document.getElementById("type").innerHTML = "Type";
        document.getElementById("subject").innerHTML = "Subject";
        document.getElementById("credit").innerHTML = "Credit";
        document.getElementById("grade").innerHTML = "Grade";

        document.getElementById("class_gpa").innerHTML = "Class GPA";
        document.getElementById("gpa_pect").innerHTML = "% of GPA";

        document.getElementById("table").style.border ="1px solid lightgray";
        document.getElementById("sem_4_gpa").style.background ="lightgreen";
        document.getElementById("gpa_percentage").style.background ="yellow";
        document.getElementById("table").style.background ="#fff";
        document.getElementById("result_topic").style.background ="#A8A7A7";

      }
    </script>
    
<?php 
include "inc/footer.php";
?>