<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>GPA Calculater</title>

    <style type="text/css">
      select{
        padding-left: 10px;
        border-radius: 5%;
      }
      tr{
        line-height: 30px;
      }

      td:nth-child(2){
        font-size: 13px;
      }

      td:nth-child(3n){
        text-align: center;
      }

      td:nth-child(even){
        text-align: center;
      }

      .Grading_scheme table td{
        text-align:center;
      }

      .Grading_scheme tr{
        line-height: 20px;
        font-size: 14px;
      }

      .Grading_scheme td:nth-child(even){
        text-align: left;
        padding-left: 30px;
      }

      @media(min-width: 380px){
        .nav-item{
          text-align: center;
        }
      }

    </style>
  </head>
  <body style="background-color: #0e9aa7">