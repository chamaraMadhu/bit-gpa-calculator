<nav class="navbar sticky-top navbar-expand-lg navbar-light p-0" style="background-color: #283655">
  <a class="navbar-brand text-light" href="index.php"><img class="pl-2" src="img/logo.png" width="75px">GPA Calculator</a>
  <button class="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Semester Wise
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="1st_sem.php">1<sup>st</sup> Semester</a>
          <a class="dropdown-item" href="2nd_sem.php">2<sup>nd</sup> Semester</a>
          <a class="dropdown-item" href="3rd_sem.php">3<sup>rd</sup> Semester</a>
          <a class="dropdown-item" href="4th_sem.php">4<sup>th</sup> Semester</a>
          <a class="dropdown-item" href="5th_sem.php">5<sup>th</sup> Semester</a>
          <a class="dropdown-item" href="6th_sem.php">6<sup>th</sup> Semester</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Year Wise
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="1st_year.php">1<sup>st</sup> Year</a>
          <a class="dropdown-item" href="2nd_year.php">2<sup>nd</sup> Year</a>
          <a class="dropdown-item" href="3rd_year.php">3<sup>rd</sup> Year</a>
        </div>
      </li>
      <!-- <li class="nav-item">
        <a class="nav-link text-light" href="full_degree.php">Full Degree</a>
      </li> -->
    </ul>
  </div>
</nav>