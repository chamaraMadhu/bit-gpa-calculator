<?php 
include "inc/header.php";
include "inc/navbar.php";
?>

    <div class="container mt-5 bg-light" style="min-height: 700px">
      <div class="row pt-2 pb-1" style="background-color: #FF847C ">
        <div class="col text-center ">
        <h5 class="text-center text-light">1<sup>st</sup> Semester GPA</h5></div>
      </div>

      <div class="row p-3" style="border-radius: 2px">
        <div class="col-sm-8 col-md-8">

          <div class="row p-3">
            <h5 class="text-muted mb-3">1<sup>st</sup> Semester</h5>
            <table>
                <tr>
                    <th width="350px">Subject</th>
                    <th class="text-center" width="100px">Type</th>
                    <th class="text-center" width="160px">Is 1st Sitting?</th>
                    <th class="text-center" width="100px">Grade</th>
                </tr>
                <tr>
                    <td>1. Information Systems & Technology</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_1" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_1" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_1">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>2. Computer Systems I</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_2" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_2" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_2">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>3. Web Application Development I</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_3" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_3" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_3">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>4. Communication Skills</td>
                    <td>Non-GPA</td>
                    <td></td>
                    <td>
                        <select id="sub_4" style="width:88px">
                          <option>Pass</option>
                          <option>Fail</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>5. Introductory Mathematics</td>
                    <td>Non-GPA</td>
                    <td></td>
                    <td>
                       <select id="sub_5" style="width:88px">
                          <option>Pass</option>
                          <option>Fail</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>6. Personal Computing</td>
                    <td>Non-GPA</td>
                    <td></td>
                    <td>
                        <select id="sub_6" style="width:88px">
                          <option>Pass</option>
                          <option>Fail</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><button class="btn btn-success btn-sm mt-2" onClick="gpaCal_2();" style="width:100%">Calculate</button></td>
                </tr>                
            </table>
          </div>

          <div class="row text-center" id="result_topic" >
            <div class="col">
            <h6 class="text-light pt-2 pb-1" id="heading"></h6>
            </div>
          </div>
          <div class="row pb-3" id="table">
            <div class="col">
              <table class="mt-3" align="center">
                <tr style="font-weight: 700">
                  <td id="subject" width="350"></td>
                  <td id="type" width="100"></td>
                  <td id="credit" width="160"></td>
                  <td id="grade" width="100"></td>
                </tr>
                <tr>
                  <td id="subject_1"></td>
                  <td id="type_1"></td>
                  <td id="credit_1"></td>
                  <td id="grade_1"></td>
                </tr>
                <tr>
                  <td id="subject_2"></td>
                  <td id="type_2"></td>
                  <td id="credit_2"></td>
                  <td id="grade_2"></td>
                </tr>
                <tr>
                  <td id="subject_3"></td>
                  <td id="type_3"></td>
                  <td id="credit_3"></td>
                  <td id="grade_3"></td>
                </tr>
                <tr>
                  <td id="subject_4"></td>
                  <td id="type_4"></td>
                  <td id="credit_4"></td>
                  <td id="grade_4"></td>
                </tr>
                <tr>
                  <td id="subject_5"></td>
                  <td id="type_5"></td>
                  <td id="credit_5"></td>
                  <td id="grade_5"></td>
                </tr>
                <tr>
                  <td id="subject_6"></td>
                  <td id="type_6"></td>
                  <td id="credit_6"></td>
                  <td id="grade_6"></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td id="class_gpa" style="font-size: 14px; font-weight: 500"></td>
                  <td id="sem_1_gpa" style="font-size: 14px; font-weight: 500"></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td id="gpa_pect" style="font-size: 14px; font-weight: 500"></td>
                  <td id="gpa_percentage" style="font-size: 14px; font-weight: 500"></td>
                </tr>
              </table>
            </div>
          </div>

        </div>

        <div class="col-sm-4 col-md-4 Grading_scheme p-3">
          <h5 class="text-muted mb-3 text-center">Grading Scheme</h5>
          <table border="1" align="center">
              <tr>
                  <th class="text-center" width="75px">Marks</th>
                  <th class="text-center" width="80px">Grade</th>
                  <th class="text-center" width="100px">Point Value</th>
              </tr>
              <tr>
                <td>90-100</td>
                <td>A+</td>
                <td>4.00</td>
              </tr>
              <tr>
                <td>80-89</td>
                <td>A</td>
                <td>4.00</td>
              </tr>
              <tr>
                <td>75-79</td>
                <td>A-</td>
                <td>3.67</td>
              </tr>
              <tr>
                <td>70-74</td>
                <td>B+</td>
                <td>3.33</td>
              </tr>
              <tr>
                <td>65-69</td>
                <td>B</td>
                <td>3.00</td>
              </tr>
              <tr>
                <td>60-64</td>
                <td>B-</td>
                <td>2.67</td>
              </tr>
              <tr>
                <td>55-59</td>
                <td>C+</td>
                <td>2.33</td>
              </tr>
              <tr>
                <td>50-54</td>
                <td>C</td>
                <td>2.00</td>
              </tr>
              <tr>
                <td>45-49</td>
                <td>C-</td>
                <td>1.67</td>
              </tr>
              <tr>
                <td>40-44</td>
                <td>D+</td>
                <td>1.33</td>
              </tr>
              <tr>
                <td>30-39</td>
                <td>D</td>
                <td>1.00</td>
              </tr>
              <tr>
                <td>20-29</td>
                <td>D-</td>
                <td>0.67</td>
              </tr>
              <tr>
                <td>0-19</td>
                <td>E</td>
                <td>0.00</td>
              </tr>
              <tr>
                <td>-</td>
                <td style="padding-left: 0px; text-align: center">Not Sat</td>
                <td>0.00</td>
              </tr>
          </table>
          <div style="font-size: 14px">
            <p class="text-center mt-3"><b class="text-danger">**</b> Not Sat = You don't Still face the subject or skipped</p>
            <p class="text-center mt-0"><b class="text-danger">**</b> Non-GPA = Not Applicable for GPA calculation</p>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      function gpaCal_2(){

        var grade_1 = document.getElementById('sub_1').value;
        var grade_2 = document.getElementById('sub_2').value;
        var grade_3 = document.getElementById('sub_3').value;
        var grade_4 = document.getElementById('sub_4').value;
        var grade_5 = document.getElementById('sub_5').value;
        var grade_6 = document.getElementById('sub_6').value;

        var sub_1;
        var sub_2;
        var sub_3;
        var sub_4;
        var sub_5;
        var sub_6;

        if(document.getElementsByName('sitting_sub_1')[0].checked){

            switch(grade_1){
              case 'Not Sat':
                sub_1 = 0;
                break;
              case 'A+':
                sub_1 = 4;
                break;
              case 'A':
                sub_1 = 4;
                break;
              case 'A-':
                sub_1 = 3.67;
                break;
              case 'B+':
                sub_1 = 3.33;
                break;
              case 'B':
                sub_1 = 3;
                break;
              case 'B-':
                sub_1 = 2.67;
                break;
              case 'C+':
                sub_1 = 2.33;
                break;
              case 'C':
                sub_1 = 2;
                break;
              case 'C-':
                sub_1 = 1.67;
                break;
              case 'D+':
                sub_1 = 1.33;
                break;
              case 'D':
                sub_1 = 1;
                break;
              case 'D-':
                sub_1 = .67;
                break;
              case 'E':
                sub_1 = 0;
                break;
            }

        }else{
          switch(grade_1){
              case 'C-':
                sub_1 = 1.67;
                break;
              case 'D+':
                sub_1 = 1.33;
                break;
              case 'D':
                sub_1 = 1;
                break;
              case 'D-':
                sub_1 = .67;
                break;
              case 'E':
                sub_1 = 0;
                break;
              default :
                sub_1 = 2;
            }
          
        }

        if(document.getElementsByName('sitting_sub_2')[0].checked){
          
            switch(grade_2){
              case 'Not Sat':
                sub_2 = 0;
                break;
              case 'A+':
                sub_2 = 4;
                break;
              case 'A':
                sub_2 = 4;
                break;
              case 'A-':
                sub_2 = 3.67;
                break;
              case 'B+':
                sub_2 = 3.33;
                break;
              case 'B':
                sub_2 = 3;
                break;
              case 'B-':
                sub_2 = 2.67;
                break;
              case 'C+':
                sub_2 = 2.33;
                break;
              case 'C':
                sub_2 = 2;
                break;
              case 'C-':
                sub_2 = 1.67;
                break;
              case 'D+':
                sub_2 = 1.33;
                break;
              case 'D':
                sub_2 = 1;
                break;
              case 'D-':
                sub_2 = .67;
                break;
              case 'E':
                sub_2 = 0;
                break;
            }

        }else{
          switch(grade_2){
              case 'C-':
                sub_2 = 1.67;
                break;
              case 'D+':
                sub_2 = 1.33;
                break;
              case 'D':
                sub_2 = 1;
                break;
              case 'D-':
                sub_2 = .67;
                break;
              case 'E':
                sub_2 = 0;
                break;
              default :
                sub_2 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_3')[0].checked){
          
            switch(grade_3){
              case 'Not Sat':
                sub_3 = 0;
                break;
              case 'A+':
                sub_3 = 4;
                break;
              case 'A':
                sub_3 = 4;
                break;
              case 'A-':
                sub_3 = 3.67;
                break;
              case 'B+':
                sub_3 = 3.33;
                break;
              case 'B':
                sub_3 = 3;
                break;
              case 'B-':
                sub_3 = 2.67;
                break;
              case 'C+':
                sub_3 = 2.33;
                break;
              case 'C':
                sub_3 = 2;
                break;
              case 'C-':
                sub_3 = 1.67;
                break;
              case 'D+':
                sub_3 = 1.33;
                break;
              case 'D':
                sub_3 = 1;
                break;
              case 'D-':
                sub_3 = .67;
                break;
              case 'E':
                sub_3 = 0;
                break;
            }

        }else{
          switch(grade_3){
              case 'C-':
                sub_3 = 1.67;
                break;
              case 'D+':
                sub_3 = 1.33;
                break;
              case 'D':
                sub_3 = 1;
                break;
              case 'D-':
                sub_3 = .67;
                break;
              case 'E':
                sub_3 = 0;
                break;
              default :
                sub_3 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_3')[0].checked){
          
            switch(grade_3){
              case 'Not Sat':
                sub_3 = 0;
                break;
              case 'A+':
                sub_3 = 4;
                break;
              case 'A':
                sub_3 = 4;
                break;
              case 'A-':
                sub_3 = 3.67;
                break;
              case 'B+':
                sub_3 = 3.33;
                break;
              case 'B':
                sub_3 = 3;
                break;
              case 'B-':
                sub_3 = 2.67;
                break;
              case 'C+':
                sub_3 = 2.33;
                break;
              case 'C':
                sub_3 = 2;
                break;
              case 'C-':
                sub_3 = 1.67;
                break;
              case 'D+':
                sub_3 = 1.33;
                break;
              case 'D':
                sub_3 = 1;
                break;
              case 'D-':
                sub_3 = .67;
                break;
              case 'E':
                sub_3 = 0;
                break;
            }

        }else{
          switch(grade_3){
              case 'C-':
                sub_3 = 1.67;
                break;
              case 'D+':
                sub_3 = 1.33;
                break;
              case 'D':
                sub_3 = 1;
                break;
              case 'D-':
                sub_3 = .67;
                break;
              case 'E':
                sub_3 = 0;
                break;
              default :
                sub_3 = 2;
            }
        }

        var gpa = ((sub_1*3)+(sub_2*3)+(sub_3*3))/9;
        document.getElementById("sem_1_gpa").innerHTML = gpa.toFixed(2);

        var gpa_percentage = (gpa/4)*100;
        document.getElementById("gpa_percentage").innerHTML = gpa_percentage.toFixed(2)+"%";

        document.getElementById("heading").innerHTML = "Result Sheet";
        document.getElementById("subject_1").innerHTML = "1. Information Systems & Technology";
        document.getElementById("subject_2").innerHTML = "2. Computer Systems I";
        document.getElementById("subject_3").innerHTML = "3. Web Application Development I";
        document.getElementById("subject_4").innerHTML = "4. Communication Skills";
        document.getElementById("subject_5").innerHTML = "5. Introductory Mathematics";
        document.getElementById("subject_6").innerHTML = "6. Personal Computing";

        document.getElementById("type_1").innerHTML = "GPA";
        document.getElementById("type_2").innerHTML = "GPA";
        document.getElementById("type_3").innerHTML = "GPA";
        document.getElementById("type_4").innerHTML = "Non-GPA";
        document.getElementById("type_5").innerHTML = "Non-GPA";
        document.getElementById("type_6").innerHTML = "Non-GPA";

        document.getElementById("credit_1").innerHTML = "3";
        document.getElementById("credit_2").innerHTML = "3";
        document.getElementById("credit_3").innerHTML = "3";
        document.getElementById("credit_4").innerHTML = "2";
        document.getElementById("credit_5").innerHTML = "2";
        document.getElementById("credit_6").innerHTML = "2";

        document.getElementById("grade_1").innerHTML = grade_1;
        document.getElementById("grade_2").innerHTML = grade_2;
        document.getElementById("grade_3").innerHTML = grade_3;
        document.getElementById("grade_4").innerHTML = grade_4;
        document.getElementById("grade_5").innerHTML = grade_5;
        document.getElementById("grade_6").innerHTML = grade_6;

        document.getElementById("type").innerHTML = "Type";
        document.getElementById("subject").innerHTML = "Subject";
        document.getElementById("credit").innerHTML = "Credit";
        document.getElementById("grade").innerHTML = "Grade";

        document.getElementById("class_gpa").innerHTML = "Class GPA";
        document.getElementById("gpa_pect").innerHTML = "% of GPA";

        document.getElementById("table").style.border ="1px solid lightgray";
        document.getElementById("sem_1_gpa").style.background ="lightgreen";
        document.getElementById("gpa_percentage").style.background ="yellow";
        document.getElementById("table").style.background ="#fff";
        document.getElementById("result_topic").style.background ="#A8A7A7";

      }
    </script>

<?php 
include "inc/footer.php";
?>