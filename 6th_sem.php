<?php 
include "inc/header.php";
include "inc/navbar.php";
?>

  <div class="container mt-5 bg-light" style="min-height: 700px">
      <div class="row pt-2 pb-1" style="background-color: #FF847C ">
        <div class="col text-center ">
        <h5 class="text-center text-light">6<sup>th</sup> Semester GPA</h5></div>
      </div>

      <div class="row bg-white p-3" style="border-radius: 2px">
        <div class="col-sm-8 col-md-8">

          <div class="row p-3">
            <h5 class="text-muted mb-3">6<sup>th</sup> Semester</h5>
            <table>
                <tr>
                    <tr>
                    <th width="350px">Subject</th>
                    <th class="text-center" width="100px">Type</th>
                    <th class="text-center" width="160px">Is 1st Sitting?</th>
                    <th class="text-center" width="100px">Grade</th>
                </tr>
                </tr>
                <tr>
                    <td>24. Software Development Project</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_24" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_24" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_24">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>25. Systems & Network Administration</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_25" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_25" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_25">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>26. e-Business Application</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_26" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_26" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_26">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>27. Database Systems II</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_27" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_27" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_27">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>28. Introduction to Entrepreneurship</td>
                    <td>Non-GPA</td>
                    <td></td>
                    <td>
                      <select id="sub_28" style="width:88px">
                          <option>Pass</option>
                          <option>Fail</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><button class="btn btn-success btn-sm mt-3" onClick="gpaCal_6();" style="width:100%">Calculate</button></td>
                </tr>
            </table>
          </div>

          <div class="row text-center" id="result_topic" >
            <div class="col">
            <h6 class="text-light pt-2 pb-1" id="heading"></h6>
            </div>
          </div>
          <div class="row pb-3" id="table">
            <div class="col">
              <table class="mt-3" align="center">
                <tr style="font-weight: 700">
                  <td id="subject" width="350"></td>
                  <td id="type" width="100"></td>
                  <td id="credit" width="160"></td>
                  <td id="grade" width="100"></td>
                </tr>
                <tr>
                  <td id="subject_24"></td>
                  <td id="type_24"></td>
                  <td id="credit_24"></td>
                  <td id="grade_24"></td>
                </tr>
                <tr>
                  <td id="subject_25"></td>
                  <td id="type_25"></td>
                  <td id="credit_25"></td>
                  <td id="grade_25"></td>
                </tr>
                <tr>
                  <td id="subject_26"></td>
                  <td id="type_26"></td>
                  <td id="credit_26"></td>
                  <td id="grade_26"></td>
                </tr>
                <tr>
                  <td id="subject_27"></td>
                  <td id="type_27"></td>
                  <td id="credit_27"></td>
                  <td id="grade_27"></td>
                </tr>
                <tr>
                  <td id="subject_28"></td>
                  <td id="type_28"></td>
                  <td id="credit_28"></td>
                  <td id="grade_28"></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td id="class_gpa" style="font-size: 14px; font-weight: 500"></td>
                  <td id="sem_6_gpa" style="font-size: 14px; font-weight: 500"></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td id="gpa_pect" style="font-size: 14px; font-weight: 500"></td>
                  <td id="gpa_percentage" style="font-size: 14px; font-weight: 500"></td>
                </tr>
              </table>
            </div>
          </div>

        </div>

        <div class="col-sm-4 col-md-4 Grading_scheme p-3">
          <h5 class="text-muted text-center mb-3">Grading Scheme</h5>
          <table align="center" border="1">
              <tr>
                  <th class="text-center" width="75px">Marks</th>
                  <th class="text-center" width="80px">Grade</th>
                  <th class="text-center" width="100px">Point Value</th>
              </tr>
              <tr>
                <td>90-100</td>
                <td>A+</td>
                <td>4.00</td>
              </tr>
              <tr>
                <td>80-89</td>
                <td>A</td>
                <td>4.00</td>
              </tr>
              <tr>
                <td>75-79</td>
                <td>A-</td>
                <td>3.67</td>
              </tr>
              <tr>
                <td>70-74</td>
                <td>B+</td>
                <td>3.33</td>
              </tr>
              <tr>
                <td>65-69</td>
                <td>B</td>
                <td>3.00</td>
              </tr>
              <tr>
                <td>60-64</td>
                <td>B-</td>
                <td>2.67</td>
              </tr>
              <tr>
                <td>55-59</td>
                <td>C+</td>
                <td>2.33</td>
              </tr>
              <tr>
                <td>50-54</td>
                <td>C</td>
                <td>2.00</td>
              </tr>
              <tr>
                <td>45-49</td>
                <td>C-</td>
                <td>1.67</td>
              </tr>
              <tr>
                <td>40-44</td>
                <td>D+</td>
                <td>1.33</td>
              </tr>
              <tr>
                <td>30-39</td>
                <td>D</td>
                <td>1.00</td>
              </tr>
              <tr>
                <td>20-29</td>
                <td>D-</td>
                <td>0.67</td>
              </tr>
              <tr>
                <td>0-19</td>
                <td>E</td>
                <td>0.00</td>
              </tr>
              <tr>
                <td>-</td>
                <td style="padding-left: 0px; text-align: center">Not Sat</td>
                <td>0.00</td>
              </tr>
          </table>
          <div style="font-size: 14px">
            <p class="text-center mt-3"><b class="text-danger">**</b> Not Sat = You don't Still face the subject or skipped</p>
            <p class="text-center mt-0"><b class="text-danger">**</b> Non-GPA = Not Applicable for GPA calculation</p>
          </div>
        </div>

      </div>
    </div>

    <script type="text/javascript">
      function gpaCal_6(){

        var grade_24 = document.getElementById('sub_24').value;
        var grade_25 = document.getElementById('sub_25').value;
        var grade_26 = document.getElementById('sub_26').value;
        var grade_27 = document.getElementById('sub_27').value;
        var grade_28 = document.getElementById('sub_28').value;

        var sub_24;
        var sub_25;
        var sub_26;
        var sub_27;
        var sub_28;

        if(document.getElementsByName('sitting_sub_24')[0].checked){

            switch(grade_24){
              case 'Not Sat':
                sub_24 = 0;
                break;
              case 'A+':
                sub_24 = 4;
                break;
              case 'A':
                sub_24 = 4;
                break;
              case 'A-':
                sub_24 = 3.67;
                break;
              case 'B+':
                sub_24 = 3.33;
                break;
              case 'B':
                sub_24 = 3;
                break;
              case 'B-':
                sub_24 = 2.67;
                break;
              case 'C+':
                sub_24 = 2.33;
                break;
              case 'C':
                sub_24 = 2;
                break;
              case 'C-':
                sub_24 = 1.67;
                break;
              case 'D+':
                sub_24 = 1.33;
                break;
              case 'D':
                sub_24 = 1;
                break;
              case 'D-':
                sub_24 = .67;
                break;
              case 'E':
                sub_24 = 0;
                break;
            }

        }else{
          switch(grade_24){
              case 'C-':
                sub_24 = 1.67;
                break;
              case 'D+':
                sub_24 = 1.33;
                break;
              case 'D':
                sub_24 = 1;
                break;
              case 'D-':
                sub_24 = .67;
                break;
              case 'E':
                sub_24 = 0;
                break;
              default :
                sub_24 = 2;
            }
          
        }

        if(document.getElementsByName('sitting_sub_25')[0].checked){
          
            switch(grade_25){
              case 'Not Sat':
                sub_25 = 0;
                break;
              case 'A+':
                sub_25 = 4;
                break;
              case 'A':
                sub_25 = 4;
                break;
              case 'A-':
                sub_25 = 3.67;
                break;
              case 'B+':
                sub_25 = 3.33;
                break;
              case 'B':
                sub_25 = 3;
                break;
              case 'B-':
                sub_25 = 2.67;
                break;
              case 'C+':
                sub_25 = 2.33;
                break;
              case 'C':
                sub_25 = 2;
                break;
              case 'C-':
                sub_25 = 1.67;
                break;
              case 'D+':
                sub_25 = 1.33;
                break;
              case 'D':
                sub_25 = 1;
                break;
              case 'D-':
                sub_25 = .67;
                break;
              case 'E':
                sub_25 = 0;
                break;
            }

        }else{
          switch(grade_25){
              case 'C-':
                sub_25 = 1.67;
                break;
              case 'D+':
                sub_25 = 1.33;
                break;
              case 'D':
                sub_25 = 1;
                break;
              case 'D-':
                sub_25 = .67;
                break;
              case 'E':
                sub_25 = 0;
                break;
              default :
                sub_25 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_26')[0].checked){
          
            switch(grade_26){
              case 'Not Sat':
                sub_26 = 0;
                break;
              case 'A+':
                sub_26 = 4;
                break;
              case 'A':
                sub_26 = 4;
                break;
              case 'A-':
                sub_26 = 3.67;
                break;
              case 'B+':
                sub_26 = 3.33;
                break;
              case 'B':
                sub_26 = 3;
                break;
              case 'B-':
                sub_26 = 2.67;
                break;
              case 'C+':
                sub_26 = 2.33;
                break;
              case 'C':
                sub_26 = 2;
                break;
              case 'C-':
                sub_26 = 1.67;
                break;
              case 'D+':
                sub_26 = 1.33;
                break;
              case 'D':
                sub_26 = 1;
                break;
              case 'D-':
                sub_26 = .67;
                break;
              case 'E':
                sub_26 = 0;
                break;
            }

        }else{
          switch(grade_26){
              case 'C-':
                sub_26 = 1.67;
                break;
              case 'D+':
                sub_26 = 1.33;
                break;
              case 'D':
                sub_26 = 1;
                break;
              case 'D-':
                sub_26 = .67;
                break;
              case 'E':
                sub_26 = 0;
                break;
              default :
                sub_26 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_27')[0].checked){
          
            switch(grade_27){
              case 'Not Sat':
                sub_27 = 0;
                break;
              case 'A+':
                sub_27 = 4;
                break;
              case 'A':
                sub_27 = 4;
                break;
              case 'A-':
                sub_27 = 3.67;
                break;
              case 'B+':
                sub_27 = 3.33;
                break;
              case 'B':
                sub_27 = 3;
                break;
              case 'B-':
                sub_27 = 2.67;
                break;
              case 'C+':
                sub_27 = 2.33;
                break;
              case 'C':
                sub_27 = 2;
                break;
              case 'C-':
                sub_27 = 1.67;
                break;
              case 'D+':
                sub_27 = 1.33;
                break;
              case 'D':
                sub_27 = 1;
                break;
              case 'D-':
                sub_27 = .67;
                break;
              case 'E':
                sub_27 = 0;
                break;
            }

        }else{
          switch(grade_27){
              case 'C-':
                sub_27 = 1.67;
                break;
              case 'D+':
                sub_27 = 1.33;
                break;
              case 'D':
                sub_27 = 1;
                break;
              case 'D-':
                sub_27 = .67;
                break;
              case 'E':
                sub_27 = 0;
                break;
              default :
                sub_27= 2;
            }
        }

        var gpa = ((sub_24*8)+(sub_25*3)+(sub_26*3)+(sub_27*3))/20;
        document.getElementById("sem_6_gpa").innerHTML = gpa.toFixed(2);

        var gpa_percentage = (gpa/4)*100;
        document.getElementById("gpa_percentage").innerHTML = gpa_percentage.toFixed(2)+"%";

        document.getElementById("heading").innerHTML = "Result Sheet";
        document.getElementById("subject_24").innerHTML = "24. Software Development Project";
        document.getElementById("subject_25").innerHTML = "25. Systems & Network Administration";
        document.getElementById("subject_26").innerHTML = "26. e-Business Application";
        document.getElementById("subject_27").innerHTML = "27. Database Systems II";
        document.getElementById("subject_28").innerHTML = "28. Introduction to Entrepreneurship";

        document.getElementById("type_24").innerHTML = "GPA";
        document.getElementById("type_25").innerHTML = "GPA";
        document.getElementById("type_26").innerHTML = "GPA";
        document.getElementById("type_27").innerHTML = "GPA";
        document.getElementById("type_28").innerHTML = "Non-GPA";

        document.getElementById("credit_24").innerHTML = "8";
        document.getElementById("credit_25").innerHTML = "3";
        document.getElementById("credit_26").innerHTML = "3";
        document.getElementById("credit_27").innerHTML = "3";
        document.getElementById("credit_28").innerHTML = "2";

        document.getElementById("grade_24").innerHTML = grade_24;
        document.getElementById("grade_25").innerHTML = grade_25;
        document.getElementById("grade_26").innerHTML = grade_26;
        document.getElementById("grade_27").innerHTML = grade_27;
        document.getElementById("grade_28").innerHTML = grade_28;

        document.getElementById("type").innerHTML = "Type";
        document.getElementById("subject").innerHTML = "Subject";
        document.getElementById("credit").innerHTML = "Credit";
        document.getElementById("grade").innerHTML = "Grade";

        document.getElementById("class_gpa").innerHTML = "Class GPA";
        document.getElementById("gpa_pect").innerHTML = "% of GPA";

        document.getElementById("table").style.border ="1px solid lightgray";
        document.getElementById("sem_6_gpa").style.background ="lightgreen";
        document.getElementById("gpa_percentage").style.background ="yellow";
        document.getElementById("table").style.background ="#fff";
        document.getElementById("result_topic").style.background ="#A8A7A7";

      }
    </script>
    
<?php 
include "inc/footer.php";
?>