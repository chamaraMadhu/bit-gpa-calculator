<?php 
include "inc/header.php";
include "inc/navbar.php";
?>

    <div class="container mt-5 bg-light" style="min-height: 700px">
      <div class="row pt-2 pb-1" style="background-color: #FF847C ">
        <div class="col text-center ">
        <h5 class="text-center text-light">Full Degree GPA</h5></div>
      </div>
      <div class="row bg-light p-3" style="border-radius: 2px">
        <div class="col-sm-8 col-md-8">

          <div class="row p-3">
            <h5 class="text-muted mb-3">1<sup>st</sup> Semester</h5>
            <table>
                <tr>
                    <th width="350px">Subject</th>
                    <th class="text-center" width="100px">Type</th>
                    <th class="text-center" width="160px">Is 1st Sitting?</th>
                    <th class="text-center" width="100px">Grade</th>
                </tr>
                <tr>
                    <td>1. Information Systems & Technology</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_1" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_1" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_1">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>2. Computer Systems I</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_2" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_2" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_2">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>3. Web Application Development I</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_3" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_3" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_3">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>4. Communication Skills</td>
                    <td>Non-GPA</td>
                    <td></td>
                    <td>
                        <select id="sub_4" style="width:88px">
                          <option>Pass</option>
                          <option>Fail</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>5. Introductory Mathematics</td>
                    <td>Non-GPA</td>
                    <td></td>
                    <td>
                       <select id="sub_5" style="width:88px">
                          <option>Pass</option>
                          <option>Fail</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>6. Personal Computing</td>
                    <td>Non-GPA</td>
                    <td></td>
                    <td>
                        <select id="sub_6" style="width:88px">
                          <option>Pass</option>
                          <option>Fail</option>
                        </select>
                    </td>
                </tr>               
            </table>
          </div>

          <div class="row p-3">
            <h5 class="text-muted mb-3">2<sup>nd</sup> Semester</h5>
            <table>
                <tr>
                    <th width="350px">Subject</th>
                    <th class="text-center" width="100px">Type</th>
                    <th class="text-center" width="160px">Is 1st Sitting?</th>
                    <th class="text-center" width="100px">Grade</th>
                </tr>
                <tr>
                    <td>7. Mathematics for Computing I</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_7" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_7" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_7">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>8. Programming I</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_8" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_8" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_8">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>9. Database Systems I</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_9" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_9" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_9">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>10. Systems Analysis & Design</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_10" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_10" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_10">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
              </table>
          </div>

          <div class="row p-3">
            <h5 class="text-muted mb-3">3<sup>rd</sup> Semester</h5>
            <table>
                <tr>
                    <th width="350px">Subject</th>
                    <th class="text-center" width="100px">Type</th>
                    <th class="text-center" width="160px">Is 1st Sitting?</th>
                    <th class="text-center" width="100px">Grade</th>
                </tr>
                <tr>
                    <td>11. Object Oriented Analysis & Design</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_11" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_11" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_11">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>12. Fundamentals of Software Engineering</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_12" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_12" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_12">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>13. Mathematics for Computing II</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_13" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_13" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_13">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>14. User Interface Design</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_14" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_14" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_14">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>15. Web Application Development II</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_15" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_15" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_15">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
            </table>
          </div>

          <div class="row p-3">
            <h5 class="text-muted mb-3">4<sup>th</sup> Semester</h5>
            <table>
                <tr>
                    <th width="350px">Subject</th>
                    <th class="text-center" width="100px">Type</th>
                    <th class="text-center" width="160px">Is 1st Sitting?</th>
                    <th class="text-center" width="100px">Grade</th>
                </tr>
                <tr>
                    <td>16. Programming II</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_16" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_16" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_16">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>17. Information Technology Project Mgt.</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_17" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_17" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_17">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>18. Rapid Software Development</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_18" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_18" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_18">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>19. Computer Networks</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_19" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_19" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_19">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
            </table>
          </div>

          <div class="row p-3">
            <h5 class="text-muted mb-3">5<sup>th</sup> Semester</h5>
            <table>
                <tr>
                    <th width="350px">Subject</th>
                    <th class="text-center" width="100px">Type</th>
                    <th class="text-center" width="160px">Is 1st Sitting?</th>
                    <th class="text-center" width="100px">Grade</th>
                </tr>
                <tr>
                    <td>20. Professional Issues in IT</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_20" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_20" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_20">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>21. Information Systems Security</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_21" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_21" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_21">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>22. Fundamentals of Multimedia</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_22" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_22" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_22">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>23. Fundamentals of Management</td>
                    <td>Nn-GPA</td>
                    <td></td>
                    <td>
                      <select id="sub_23" style="width:88px">
                          <option>Pass</option>
                          <option>Fail</option>
                        </select>
                    </td>
                </tr>
            </table>
          </div>

          <div class="row p-3">
            <h5 class="text-muted mb-3">6<sup>th</sup> Semester</h5>
            <table>
                <tr>
                    <tr>
                    <th width="350px">Subject</th>
                    <th class="text-center" width="100px">Type</th>
                    <th class="text-center" width="160px">Is 1st Sitting?</th>
                    <th class="text-center" width="100px">Grade</th>
                </tr>
                </tr>
                <tr>
                    <td>24. Software Development Project</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_24" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_24" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_24">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>25. Systems & Network Administration</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_25" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_25" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_25">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>26. e-Business Application</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_26" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_26" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_26">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>27. Database Systems II</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_27" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_27" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_27">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>28. Introduction to Entrepreneurship</td>
                    <td>Non-GPA</td>
                    <td></td>
                    <td>
                      <select id="sub_28" style="width:88px">
                          <option>Pass</option>
                          <option>Fail</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><button class="btn btn-success btn-sm mt-3" onClick="gpaCal_degree();" style="width:100%">Calculate</button></td>
                </tr>
            </table>
          </div>

          <div class="row text-center" id="result_topic">
            <div class="col">
            <h6 class="text-light pt-2 pb-1" id="heading"></h6>
            </div>
          </div>
          <div class="row pb-3" id="table">
            <div class="col">
              <table class="mt-3" align="center">
                <tr style="font-weight: 700">
                  <td id="subject" width="350"></td>
                  <td id="type" width="100"></td>
                  <td id="credit" width="160"></td>
                  <td id="grade" width="100"></td>
                </tr>
                <tr>
                  <td id="subject_1"></td>
                  <td id="type_1"></td>
                  <td id="credit_1"></td>
                  <td id="grade_1"></td>
                </tr>
                <tr>
                  <td id="subject_2"></td>
                  <td id="type_2"></td>
                  <td id="credit_2"></td>
                  <td id="grade_2"></td>
                </tr>
                <tr>
                  <td id="subject_3"></td>
                  <td id="type_3"></td>
                  <td id="credit_3"></td>
                  <td id="grade_3"></td>
                </tr>
                <tr>
                  <td id="subject_4"></td>
                  <td id="type_4"></td>
                  <td id="credit_4"></td>
                  <td id="grade_4"></td>
                </tr>
                <tr>
                  <td id="subject_5"></td>
                  <td id="type_5"></td>
                  <td id="credit_5"></td>
                  <td id="grade_5"></td>
                </tr>
                <tr>
                  <td id="subject_6"></td>
                  <td id="type_6"></td>
                  <td id="credit_6"></td>
                  <td id="grade_6"></td>
                </tr>
                <tr>
                  <td id="subject_7"></td>
                  <td id="type_7"></td>
                  <td id="credit_7"></td>
                  <td id="grade_7"></td>
                </tr>
                <tr>
                  <td id="subject_8"></td>
                  <td id="type_8"></td>
                  <td id="credit_8"></td>
                  <td id="grade_8"></td>
                </tr>
                <tr>
                  <td id="subject_9"></td>
                  <td id="type_9"></td>
                  <td id="credit_9"></td>
                  <td id="grade_9"></td>
                </tr>
                <tr>
                  <td id="subject_10"></td>
                  <td id="type_10"></td>
                  <td id="credit_10"></td>
                  <td id="grade_10"></td>
                </tr>
                <tr>
                  <td id="subject_11"></td>
                  <td id="type_11"></td>
                  <td id="credit_11"></td>
                  <td id="grade_11"></td>
                </tr>
                <tr>
                  <td id="subject_12"></td>
                  <td id="type_12"></td>
                  <td id="credit_12"></td>
                  <td id="grade_12"></td>
                </tr>
                <tr>
                  <td id="subject_13"></td>
                  <td id="type_13"></td>
                  <td id="credit_13"></td>
                  <td id="grade_13"></td>
                </tr>
                <tr>
                  <td id="subject_14"></td>
                  <td id="type_14"></td>
                  <td id="credit_14"></td>
                  <td id="grade_14"></td>
                </tr>
                <tr>
                  <td id="subject_15"></td>
                  <td id="type_15"></td>
                  <td id="credit_15"></td>
                  <td id="grade_15"></td>
                </tr>
                <tr>
                  <td id="subject_16"></td>
                  <td id="type_16"></td>
                  <td id="credit_16"></td>
                  <td id="grade_16"></td>
                </tr>
                <tr>
                  <td id="subject_17"></td>
                  <td id="type_17"></td>
                  <td id="credit_17"></td>
                  <td id="grade_17"></td>
                </tr>
                <tr>
                  <td id="subject_18"></td>
                  <td id="type_18"></td>
                  <td id="credit_18"></td>
                  <td id="grade_18"></td>
                </tr>
                <tr>
                  <td id="subject_19"></td>
                  <td id="type_19"></td>
                  <td id="credit_19"></td>
                  <td id="grade_19"></td>
                </tr>
                <tr>
                  <td id="subject_20"></td>
                  <td id="type_20"></td>
                  <td id="credit_20"></td>
                  <td id="grade_20"></td>
                </tr>
                <tr>
                  <td id="subject_21"></td>
                  <td id="type_21"></td>
                  <td id="credit_21"></td>
                  <td id="grade_21"></td>
                </tr>
                <tr>
                  <td id="subject_22"></td>
                  <td id="type_22"></td>
                  <td id="credit_22"></td>
                  <td id="grade_22"></td>
                </tr>
                <tr>
                  <td id="subject_23"></td>
                  <td id="type_23"></td>
                  <td id="credit_23"></td>
                  <td id="grade_23"></td>
                </tr>
                <tr>
                  <td id="subject_24"></td>
                  <td id="type_24"></td>
                  <td id="credit_24"></td>
                  <td id="grade_24"></td>
                </tr>
                <tr>
                  <td id="subject_25"></td>
                  <td id="type_25"></td>
                  <td id="credit_25"></td>
                  <td id="grade_25"></td>
                </tr>
                <tr>
                  <td id="subject_26"></td>
                  <td id="type_26"></td>
                  <td id="credit_26"></td>
                  <td id="grade_26"></td>
                </tr>
                <tr>
                  <td id="subject_27"></td>
                  <td id="type_27"></td>
                  <td id="credit_27"></td>
                  <td id="grade_27"></td>
                </tr>
                <tr>
                  <td id="subject_28"></td>
                  <td id="type_28"></td>
                  <td id="credit_28"></td>
                  <td id="grade_28"></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td id="class_gpa" style="font-size: 14px; font-weight: 500"></td>
                  <td id="full_degree_gpa" style="font-size: 14px; font-weight: 500"></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td id="gpa_pect" style="font-size: 14px; font-weight: 500"></td>
                  <td id="gpa_percentage" style="font-size: 14px; font-weight: 500"></td>
                </tr>
              </table>
            </div>
          </div>

        </div>

        <div class="col-sm-4 col-md-4 Grading_scheme p-3">
          <h5 class="text-muted text-center mb-3">Grading Scheme</h5>
          <table align="center" border="1">
              <tr>
                  <th class="text-center" width="75px">Marks</th>
                  <th class="text-center" width="80px">Grade</th>
                  <th class="text-center" width="100px">Point Value</th>
              </tr>
              <tr>
                <td>90-100</td>
                <td>A+</td>
                <td>4.00</td>
              </tr>
              <tr>
                <td>80-89</td>
                <td>A</td>
                <td>4.00</td>
              </tr>
              <tr>
                <td>75-79</td>
                <td>A-</td>
                <td>3.67</td>
              </tr>
              <tr>
                <td>70-74</td>
                <td>B+</td>
                <td>3.33</td>
              </tr>
              <tr>
                <td>65-69</td>
                <td>B</td>
                <td>3.00</td>
              </tr>
              <tr>
                <td>60-64</td>
                <td>B-</td>
                <td>2.67</td>
              </tr>
              <tr>
                <td>55-59</td>
                <td>C+</td>
                <td>2.33</td>
              </tr>
              <tr>
                <td>50-54</td>
                <td>C</td>
                <td>2.00</td>
              </tr>
              <tr>
                <td>45-49</td>
                <td>C-</td>
                <td>1.67</td>
              </tr>
              <tr>
                <td>40-44</td>
                <td>D+</td>
                <td>1.33</td>
              </tr>
              <tr>
                <td>30-39</td>
                <td>D</td>
                <td>1.00</td>
              </tr>
              <tr>
                <td>20-29</td>
                <td>D-</td>
                <td>0.67</td>
              </tr>
              <tr>
                <td>0-19</td>
                <td>E</td>
                <td>0.00</td>
              </tr>
              <tr>
                <td>-</td>
                <td style="padding-left: 0px; text-align: center">Not Sat</td>
                <td>0.00</td>
              </tr>
          </table>
          <div style="font-size: 14px">
            <p class="text-center mt-3"><b class="text-danger">**</b> Not Sat = You don't Still face the subject or skipped</p>
            <p class="text-center mt-0"><b class="text-danger">**</b> Non-GPA = Not Applicable for GPA calculation</p>
          </div>
        </div>

      </div>
    </div>

    <script type="text/javascript">
      function gpaCal_degree(){

        var grade_1 = document.getElementById('sub_1').value;
        var grade_2 = document.getElementById('sub_2').value;
        var grade_3 = document.getElementById('sub_3').value;
        var grade_4 = document.getElementById('sub_4').value;
        var grade_5 = document.getElementById('sub_5').value;
        var grade_6 = document.getElementById('sub_6').value;
        var grade_7 = document.getElementById('sub_7').value;
        var grade_8 = document.getElementById('sub_8').value;
        var grade_9 = document.getElementById('sub_9').value;
        var grade_10 = document.getElementById('sub_10').value;
        var grade_11 = document.getElementById('sub_11').value;
        var grade_12 = document.getElementById('sub_12').value;
        var grade_13 = document.getElementById('sub_13').value;
        var grade_14 = document.getElementById('sub_14').value;
        var grade_15 = document.getElementById('sub_15').value;
        var grade_16 = document.getElementById('sub_16').value;
        var grade_17 = document.getElementById('sub_17').value;
        var grade_18 = document.getElementById('sub_18').value;
        var grade_19 = document.getElementById('sub_19').value;
        var grade_20 = document.getElementById('sub_20').value;
        var grade_21 = document.getElementById('sub_21').value;
        var grade_22 = document.getElementById('sub_22').value;
        var grade_23 = document.getElementById('sub_23').value;
        var grade_24 = document.getElementById('sub_24').value;
        var grade_25 = document.getElementById('sub_25').value;
        var grade_26 = document.getElementById('sub_26').value;
        var grade_27 = document.getElementById('sub_27').value;
        var grade_28 = document.getElementById('sub_28').value;

        var sub_1;
        var sub_2;
        var sub_3;
        var sub_4;
        var sub_5;
        var sub_6;
        var sub_7;
        var sub_8;
        var sub_9;
        var sub_10;
        var sub_11;
        var sub_12;
        var sub_13;
        var sub_14;
        var sub_15;
        var sub_16;
        var sub_17;
        var sub_18;
        var sub_19;
        var sub_20;
        var sub_21;
        var sub_22;
        var sub_23;
        var sub_24;
        var sub_25;
        var sub_26;
        var sub_27;
        var sub_28;

        if(document.getElementsByName('sitting_sub_1')[0].checked){

            switch(grade_1){
              case 'Not Sat':
                sub_1 = 0;
                break;
              case 'A+':
                sub_1 = 4;
                break;
              case 'A':
                sub_1 = 4;
                break;
              case 'A-':
                sub_1 = 3.67;
                break;
              case 'B+':
                sub_1 = 3.33;
                break;
              case 'B':
                sub_1 = 3;
                break;
              case 'B-':
                sub_1 = 2.67;
                break;
              case 'C+':
                sub_1 = 2.33;
                break;
              case 'C':
                sub_1 = 2;
                break;
              case 'C-':
                sub_1 = 1.67;
                break;
              case 'D+':
                sub_1 = 1.33;
                break;
              case 'D':
                sub_1 = 1;
                break;
              case 'D-':
                sub_1 = .67;
                break;
              case 'E':
                sub_1 = 0;
                break;
            }

        }else{
          switch(grade_1){
              case 'C-':
                sub_1 = 1.67;
                break;
              case 'D+':
                sub_1 = 1.33;
                break;
              case 'D':
                sub_1 = 1;
                break;
              case 'D-':
                sub_1 = .67;
                break;
              case 'E':
                sub_1 = 0;
                break;
              default :
                sub_1 = 2;
            }
          
        }

        if(document.getElementsByName('sitting_sub_2')[0].checked){
          
            switch(grade_2){
              case 'Not Sat':
                sub_2 = 0;
                break;
              case 'A+':
                sub_2 = 4;
                break;
              case 'A':
                sub_2 = 4;
                break;
              case 'A-':
                sub_2 = 3.67;
                break;
              case 'B+':
                sub_2 = 3.33;
                break;
              case 'B':
                sub_2 = 3;
                break;
              case 'B-':
                sub_2 = 2.67;
                break;
              case 'C+':
                sub_2 = 2.33;
                break;
              case 'C':
                sub_2 = 2;
                break;
              case 'C-':
                sub_2 = 1.67;
                break;
              case 'D+':
                sub_2 = 1.33;
                break;
              case 'D':
                sub_2 = 1;
                break;
              case 'D-':
                sub_2 = .67;
                break;
              case 'E':
                sub_2 = 0;
                break;
            }

        }else{
          switch(grade_2){
              case 'C-':
                sub_2 = 1.67;
                break;
              case 'D+':
                sub_2 = 1.33;
                break;
              case 'D':
                sub_2 = 1;
                break;
              case 'D-':
                sub_2 = .67;
                break;
              case 'E':
                sub_2 = 0;
                break;
              default :
                sub_2 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_3')[0].checked){
          
            switch(grade_3){
              case 'Not Sat':
                sub_3 = 0;
                break;
              case 'A+':
                sub_3 = 4;
                break;
              case 'A':
                sub_3 = 4;
                break;
              case 'A-':
                sub_3 = 3.67;
                break;
              case 'B+':
                sub_3 = 3.33;
                break;
              case 'B':
                sub_3 = 3;
                break;
              case 'B-':
                sub_3 = 2.67;
                break;
              case 'C+':
                sub_3 = 2.33;
                break;
              case 'C':
                sub_3 = 2;
                break;
              case 'C-':
                sub_3 = 1.67;
                break;
              case 'D+':
                sub_3 = 1.33;
                break;
              case 'D':
                sub_3 = 1;
                break;
              case 'D-':
                sub_3 = .67;
                break;
              case 'E':
                sub_3 = 0;
                break;
            }

        }else{
          switch(grade_3){
              case 'C-':
                sub_3 = 1.67;
                break;
              case 'D+':
                sub_3 = 1.33;
                break;
              case 'D':
                sub_3 = 1;
                break;
              case 'D-':
                sub_3 = .67;
                break;
              case 'E':
                sub_3 = 0;
                break;
              default :
                sub_3 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_3')[0].checked){
          
            switch(grade_3){
              case 'Not Sat':
                sub_3 = 0;
                break;
              case 'A+':
                sub_3 = 4;
                break;
              case 'A':
                sub_3 = 4;
                break;
              case 'A-':
                sub_3 = 3.67;
                break;
              case 'B+':
                sub_3 = 3.33;
                break;
              case 'B':
                sub_3 = 3;
                break;
              case 'B-':
                sub_3 = 2.67;
                break;
              case 'C+':
                sub_3 = 2.33;
                break;
              case 'C':
                sub_3 = 2;
                break;
              case 'C-':
                sub_3 = 1.67;
                break;
              case 'D+':
                sub_3 = 1.33;
                break;
              case 'D':
                sub_3 = 1;
                break;
              case 'D-':
                sub_3 = .67;
                break;
              case 'E':
                sub_3 = 0;
                break;
            }

        }else{
          switch(grade_3){
              case 'C-':
                sub_3 = 1.67;
                break;
              case 'D+':
                sub_3 = 1.33;
                break;
              case 'D':
                sub_3 = 1;
                break;
              case 'D-':
                sub_3 = .67;
                break;
              case 'E':
                sub_3 = 0;
                break;
              default :
                sub_3 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_7')[0].checked){

            switch(grade_7){
              case 'Not Sat':
                sub_7 = 0;
                break;
              case 'A+':
                sub_7 = 4;
                break;
              case 'A':
                sub_7 = 4;
                break;
              case 'A-':
                sub_7 = 3.67;
                break;
              case 'B+':
                sub_7 = 3.33;
                break;
              case 'B':
                sub_7 = 3;
                break;
              case 'B-':
                sub_7 = 2.67;
                break;
              case 'C+':
                sub_7 = 2.33;
                break;
              case 'C':
                sub_7 = 2;
                break;
              case 'C-':
                sub_7 = 1.67;
                break;
              case 'D+':
                sub_7 = 1.33;
                break;
              case 'D':
                sub_7 = 1;
                break;
              case 'D-':
                sub_7 = .67;
                break;
              case 'E':
                sub_7 = 0;
                break;
            }

        }else{
          switch(grade_7){
              case 'C-':
                sub_7 = 1.67;
                break;
              case 'D+':
                sub_7 = 1.33;
                break;
              case 'D':
                sub_7 = 1;
                break;
              case 'D-':
                sub_7 = .67;
                break;
              case 'E':
                sub_7 = 0;
                break;
              default :
                sub_7 = 2;
            }
          
        }

        if(document.getElementsByName('sitting_sub_8')[0].checked){
          
            switch(grade_8){
              case 'Not Sat':
                sub_8 = 0;
                break;
              case 'A+':
                sub_8 = 4;
                break;
              case 'A':
                sub_8 = 4;
                break;
              case 'A-':
                sub_8 = 3.67;
                break;
              case 'B+':
                sub_8 = 3.33;
                break;
              case 'B':
                sub_8 = 3;
                break;
              case 'B-':
                sub_8 = 2.67;
                break;
              case 'C+':
                sub_8 = 2.33;
                break;
              case 'C':
                sub_8 = 2;
                break;
              case 'C-':
                sub_8 = 1.67;
                break;
              case 'D+':
                sub_8 = 1.33;
                break;
              case 'D':
                sub_8 = 1;
                break;
              case 'D-':
                sub_8 = .67;
                break;
              case 'E':
                sub_8 = 0;
                break;
            }

        }else{
          switch(grade_8){
              case 'C-':
                sub_8 = 1.67;
                break;
              case 'D+':
                sub_8 = 1.33;
                break;
              case 'D':
                sub_8 = 1;
                break;
              case 'D-':
                sub_8 = .67;
                break;
              case 'E':
                sub_8 = 0;
                break;
              default :
                sub_8 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_9')[0].checked){
          
            switch(grade_9){
              case 'Not Sat':
                sub_9 = 0;
                break;
              case 'A+':
                sub_9 = 4;
                break;
              case 'A':
                sub_9 = 4;
                break;
              case 'A-':
                sub_9 = 3.67;
                break;
              case 'B+':
                sub_9 = 3.33;
                break;
              case 'B':
                sub_9 = 3;
                break;
              case 'B-':
                sub_9 = 2.67;
                break;
              case 'C+':
                sub_9 = 2.33;
                break;
              case 'C':
                sub_9 = 2;
                break;
              case 'C-':
                sub_9 = 1.67;
                break;
              case 'D+':
                sub_9 = 1.33;
                break;
              case 'D':
                sub_9 = 1;
                break;
              case 'D-':
                sub_9 = .67;
                break;
              case 'E':
                sub_9 = 0;
                break;
            }

        }else{
          switch(grade_9){
              case 'C-':
                sub_9 = 1.67;
                break;
              case 'D+':
                sub_9 = 1.33;
                break;
              case 'D':
                sub_9 = 1;
                break;
              case 'D-':
                sub_9 = .67;
                break;
              case 'E':
                sub_9 = 0;
                break;
              default :
                sub_9 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_10')[0].checked){
            switch(grade_10){
              case 'Not Sat':
                sub_10 = 0;
                break;
              case 'A+':
                sub_10 = 4;
                break;
              case 'A':
                sub_10 = 4;
                break;
              case 'A-':
                sub_10 = 3.67;
                break;
              case 'B+':
                sub_10 = 3.33;
                break;
              case 'B':
                sub_10 = 3;
                break;
              case 'B-':
                sub_10 = 2.67;
                break;
              case 'C+':
                sub_10 = 2.33;
                break;
              case 'C':
                sub_10 = 2;
                break;
              case 'C-':
                sub_10 = 1.67;
                break;
              case 'D+':
                sub_10 = 1.33;
                break;
              case 'D':
                sub_10= 1;
                break;
              case 'D-':
                sub_10 = .67;
                break;
              case 'E':
                sub_10 = 0;
                break;
            }

        }else{
          switch(grade_10){
              case 'C-':
                sub_10 = 1.67;
                break;
              case 'D+':
                sub_10 = 1.33;
                break;
              case 'D':
                sub_10 = 1;
                break;
              case 'D-':
                sub_10 = .67;
                break;
              case 'E':
                sub_10 = 0;
                break;
              default :
                sub_10 = 2;
            }
        }

      if(document.getElementsByName('sitting_sub_11')[0].checked){

        switch(grade_11){
              case 'Not Sat':
                sub_11 = 0;
                break;
              case 'A+':
                sub_11 = 4;
                break;
              case 'A':
                sub_11 = 4;
                break;
              case 'A-':
                sub_11 = 3.67;
                break;
              case 'B+':
                sub_11 = 3.33;
                break;
              case 'B':
                sub_11 = 3;
                break;
              case 'B-':
                sub_11 = 2.67;
                break;
              case 'C+':
                sub_11 = 2.33;
                break;
              case 'C':
                sub_11 = 2;
                break;
              case 'C-':
                sub_11 = 1.67;
                break;
              case 'D+':
                sub_11 = 1.33;
                break;
              case 'D':
                sub_11 = 1;
                break;
              case 'D-':
                sub_11 = .67;
                break;
              case 'E':
                sub_11 = 0;
                break;
            }

        }else{
          switch(grade_11){
              case 'C-':
                sub_11 = 1.67;
                break;
              case 'D+':
                sub_11 = 1.33;
                break;
              case 'D':
                sub_11 = 1;
                break;
              case 'D-':
                sub_11 = .67;
                break;
              case 'E':
                sub_11 = 0;
                break;
              default :
                sub_11 = 2;
            }
          
        }

        if(document.getElementsByName('sitting_sub_12')[0].checked){
          
            switch(grade_12){
              case 'Not Sat':
                sub_12 = 0;
                break;
              case 'A+':
                sub_12 = 4;
                break;
              case 'A':
                sub_12 = 4;
                break;
              case 'A-':
                sub_12 = 3.67;
                break;
              case 'B+':
                sub_12 = 3.33;
                break;
              case 'B':
                sub_12 = 3;
                break;
              case 'B-':
                sub_12 = 2.67;
                break;
              case 'C+':
                sub_12 = 2.33;
                break;
              case 'C':
                sub_12 = 2;
                break;
              case 'C-':
                sub_12 = 1.67;
                break;
              case 'D+':
                sub_12 = 1.33;
                break;
              case 'D':
                sub_12 = 1;
                break;
              case 'D-':
                sub_12 = .67;
                break;
              case 'E':
                sub_12 = 0;
                break;
            }

        }else{
          switch(grade_12){
              case 'C-':
                sub_12 = 1.67;
                break;
              case 'D+':
                sub_12 = 1.33;
                break;
              case 'D':
                sub_12 = 1;
                break;
              case 'D-':
                sub_12 = .67;
                break;
              case 'E':
                sub_12 = 0;
                break;
              default :
                sub_12 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_13')[0].checked){
          
            switch(grade_13){
              case 'Not Sat':
                sub_13 = 0;
                break;
              case 'A+':
                sub_13 = 4;
                break;
              case 'A':
                sub_13 = 4;
                break;
              case 'A-':
                sub_13 = 3.67;
                break;
              case 'B+':
                sub_13 = 3.33;
                break;
              case 'B':
                sub_13 = 3;
                break;
              case 'B-':
                sub_13 = 2.67;
                break;
              case 'C+':
                sub_13 = 2.33;
                break;
              case 'C':
                sub_13 = 2;
                break;
              case 'C-':
                sub_13 = 1.67;
                break;
              case 'D+':
                sub_13 = 1.33;
                break;
              case 'D':
                sub_13 = 1;
                break;
              case 'D-':
                sub_13 = .67;
                break;
              case 'E':
                sub_13 = 0;
                break;
            }

        }else{
          switch(grade_13){
              case 'C-':
                sub_13 = 1.67;
                break;
              case 'D+':
                sub_13 = 1.33;
                break;
              case 'D':
                sub_13 = 1;
                break;
              case 'D-':
                sub_13 = .67;
                break;
              case 'E':
                sub_13 = 0;
                break;
              default :
                sub_13 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_14')[0].checked){
          
            switch(grade_14){
              case 'Not Sat':
                sub_14 = 0;
                break;
              case 'A+':
                sub_14 = 4;
                break;
              case 'A':
                sub_14 = 4;
                break;
              case 'A-':
                sub_14 = 3.67;
                break;
              case 'B+':
                sub_14 = 3.33;
                break;
              case 'B':
                sub_14 = 3;
                break;
              case 'B-':
                sub_14 = 2.67;
                break;
              case 'C+':
                sub_14 = 2.33;
                break;
              case 'C':
                sub_14 = 2;
                break;
              case 'C-':
                sub_14 = 1.67;
                break;
              case 'D+':
                sub_14 = 1.33;
                break;
              case 'D':
                sub_14 = 1;
                break;
              case 'D-':
                sub_14 = .67;
                break;
              case 'E':
                sub_14 = 0;
                break;
            }

        }else{
          switch(grade_14){
              case 'C-':
                sub_14 = 1.67;
                break;
              case 'D+':
                sub_14 = 1.33;
                break;
              case 'D':
                sub_14 = 1;
                break;
              case 'D-':
                sub_14 = .67;
                break;
              case 'E':
                sub_14 = 0;
                break;
              default :
                sub_14= 2;
            }
        }

        if(document.getElementsByName('sitting_sub_15')[0].checked){
          
            switch(grade_15){
              case 'Not Sat':
                sub_15 = 0;
                break;
              case 'A+':
                sub_15 = 4;
                break;
              case 'A':
                sub_15 = 4;
                break;
              case 'A-':
                sub_15 = 3.67;
                break;
              case 'B+':
                sub_15 = 3.33;
                break;
              case 'B':
                sub_15 = 3;
                break;
              case 'B-':
                sub_15 = 2.67;
                break;
              case 'C+':
                sub_15 = 2.33;
                break;
              case 'C':
                sub_15 = 2;
                break;
              case 'C-':
                sub_15 = 1.67;
                break;
              case 'D+':
                sub_15 = 1.33;
                break;
              case 'D':
                sub_15 = 1;
                break;
              case 'D-':
                sub_15 = .67;
                break;
              case 'E':
                sub_15 = 0;
                break;
            }

        }else{
          switch(grade_15){
              case 'C-':
                sub_15 = 1.67;
                break;
              case 'D+':
                sub_15 = 1.33;
                break;
              case 'D':
                sub_15 = 1;
                break;
              case 'D-':
                sub_15 = .67;
                break;
              case 'E':
                sub_15 = 0;
                break;
              default :
                sub_15 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_16')[0].checked){

            switch(grade_16){
              case 'Not Sat':
                sub_16 = 0;
                break;
              case 'A+':
                sub_16 = 4;
                break;
              case 'A':
                sub_16 = 4;
                break;
              case 'A-':
                sub_16 = 3.67;
                break;
              case 'B+':
                sub_16 = 3.33;
                break;
              case 'B':
                sub_16 = 3;
                break;
              case 'B-':
                sub_16 = 2.67;
                break;
              case 'C+':
                sub_16 = 2.33;
                break;
              case 'C':
                sub_16 = 2;
                break;
              case 'C-':
                sub_16 = 1.67;
                break;
              case 'D+':
                sub_16= 1.33;
                break;
              case 'D':
                sub_16 = 1;
                break;
              case 'D-':
                sub_16 = .67;
                break;
              case 'E':
                sub_16 = 0;
                break;
            }

        }else{
          switch(grade_16){
              case 'C-':
                sub_16 = 1.67;
                break;
              case 'D+':
                sub_16 = 1.33;
                break;
              case 'D':
                sub_16 = 1;
                break;
              case 'D-':
                sub_16 = .67;
                break;
              case 'E':
                sub_16 = 0;
                break;
              default :
                sub_16 = 2;
            }
          
        }

        if(document.getElementsByName('sitting_sub_17')[0].checked){
          
            switch(grade_17){
              case 'Not Sat':
                sub_17 = 0;
                break;
              case 'A+':
                sub_17 = 4;
                break;
              case 'A':
                sub_17 = 4;
                break;
              case 'A-':
                sub_17 = 3.67;
                break;
              case 'B+':
                sub_17 = 3.33;
                break;
              case 'B':
                sub_17 = 3;
                break;
              case 'B-':
                sub_17 = 2.67;
                break;
              case 'C+':
                sub_17 = 2.33;
                break;
              case 'C':
                sub_17 = 2;
                break;
              case 'C-':
                sub_17 = 1.67;
                break;
              case 'D+':
                sub_17 = 1.33;
                break;
              case 'D':
                sub_17 = 1;
                break;
              case 'D-':
                sub_17 = .67;
                break;
              case 'E':
                sub_17 = 0;
                break;
            }

        }else{
          switch(grade_17){
              case 'C-':
                sub_17 = 1.67;
                break;
              case 'D+':
                sub_17 = 1.33;
                break;
              case 'D':
                sub_17 = 1;
                break;
              case 'D-':
                sub_17 = .67;
                break;
              case 'E':
                sub_17 = 0;
                break;
              default :
                sub_17 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_18')[0].checked){
          
            switch(grade_18){
              case 'Not Sat':
                sub_18 = 0;
                break;
              case 'A+':
                sub_18 = 4;
                break;
              case 'A':
                sub_18 = 4;
                break;
              case 'A-':
                sub_18 = 3.67;
                break;
              case 'B+':
                sub_18 = 3.33;
                break;
              case 'B':
                sub_18 = 3;
                break;
              case 'B-':
                sub_18 = 2.67;
                break;
              case 'C+':
                sub_18 = 2.33;
                break;
              case 'C':
                sub_18 = 2;
                break;
              case 'C-':
                sub_18 = 1.67;
                break;
              case 'D+':
                sub_18 = 1.33;
                break;
              case 'D':
                sub_18 = 1;
                break;
              case 'D-':
                sub_18 = .67;
                break;
              case 'E':
                sub_18 = 0;
                break;
            }

        }else{
          switch(grade_18){
              case 'C-':
                sub_18 = 1.67;
                break;
              case 'D+':
                sub_18 = 1.33;
                break;
              case 'D':
                sub_18 = 1;
                break;
              case 'D-':
                sub_18 = .67;
                break;
              case 'E':
                sub_18 = 0;
                break;
              default :
                sub_18 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_19')[0].checked){
          
            switch(grade_19){
              case 'Not Sat':
                sub_19 = 0;
                break;
              case 'A+':
                sub_19 = 4;
                break;
              case 'A':
                sub_19 = 4;
                break;
              case 'A-':
                sub_19 = 3.67;
                break;
              case 'B+':
                sub_19 = 3.33;
                break;
              case 'B':
                sub_19 = 3;
                break;
              case 'B-':
                sub_19 = 2.67;
                break;
              case 'C+':
                sub_19 = 2.33;
                break;
              case 'C':
                sub_19 = 2;
                break;
              case 'C-':
                sub_19 = 1.67;
                break;
              case 'D+':
                sub_19 = 1.33;
                break;
              case 'D':
                sub_19 = 1;
                break;
              case 'D-':
                sub_19 = .67;
                break;
              case 'E':
                sub_19 = 0;
                break;
            }

        }else{
          switch(grade_19){
              case 'C-':
                sub_19 = 1.67;
                break;
              case 'D+':
                sub_19 = 1.33;
                break;
              case 'D':
                sub_19 = 1;
                break;
              case 'D-':
                sub_19 = .67;
                break;
              case 'E':
                sub_19 = 0;
                break;
              default :
                sub_19= 2;
            }
        }

    if(document.getElementsByName('sitting_sub_20')[0].checked){

        switch(grade_20){
              case 'Not Sat':
                sub_20 = 0;
                break;
              case 'A+':
                sub_20 = 4;
                break;
              case 'A':
                sub_20 = 4;
                break;
              case 'A-':
                sub_20 = 3.67;
                break;
              case 'B+':
                sub_20 = 3.33;
                break;
              case 'B':
                sub_20 = 3;
                break;
              case 'B-':
                sub_20 = 2.67;
                break;
              case 'C+':
                sub_20 = 2.33;
                break;
              case 'C':
                sub_20 = 2;
                break;
              case 'C-':
                sub_20 = 1.67;
                break;
              case 'D+':
                sub_20 = 1.33;
                break;
              case 'D':
                sub_20 = 1;
                break;
              case 'D-':
                sub_20 = .67;
                break;
              case 'E':
                sub_20 = 0;
                break;
            }

        }else{
          switch(grade_20){
              case 'C-':
                sub_20 = 1.67;
                break;
              case 'D+':
                sub_20 = 1.33;
                break;
              case 'D':
                sub_20 = 1;
                break;
              case 'D-':
                sub_20 = .67;
                break;
              case 'E':
                sub_20 = 0;
                break;
              default :
                sub_20 = 2;
            }
          
        }

        if(document.getElementsByName('sitting_sub_21')[0].checked){
          
            switch(grade_21){
              case 'Not Sat':
                sub_21 = 0;
                break;
              case 'A+':
                sub_21 = 4;
                break;
              case 'A':
                sub_21 = 4;
                break;
              case 'A-':
                sub_21 = 3.67;
                break;
              case 'B+':
                sub_21 = 3.33;
                break;
              case 'B':
                sub_21 = 3;
                break;
              case 'B-':
                sub_21 = 2.67;
                break;
              case 'C+':
                sub_21 = 2.33;
                break;
              case 'C':
                sub_21 = 2;
                break;
              case 'C-':
                sub_21 = 1.67;
                break;
              case 'D+':
                sub_21 = 1.33;
                break;
              case 'D':
                sub_21 = 1;
                break;
              case 'D-':
                sub_21 = .67;
                break;
              case 'E':
                sub_21 = 0;
                break;
            }

        }else{
          switch(grade_21){
              case 'C-':
                sub_21 = 1.67;
                break;
              case 'D+':
                sub_21 = 1.33;
                break;
              case 'D':
                sub_21 = 1;
                break;
              case 'D-':
                sub_21 = .67;
                break;
              case 'E':
                sub_21 = 0;
                break;
              default :
                sub_21 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_22')[0].checked){
          
            switch(grade_22){
              case 'Not Sat':
                sub_22 = 0;
                break;
              case 'A+':
                sub_22 = 4;
                break;
              case 'A':
                sub_22 = 4;
                break;
              case 'A-':
                sub_22 = 3.67;
                break;
              case 'B+':
                sub_22 = 3.33;
                break;
              case 'B':
                sub_22 = 3;
                break;
              case 'B-':
                sub_22 = 2.67;
                break;
              case 'C+':
                sub_22 = 2.33;
                break;
              case 'C':
                sub_22 = 2;
                break;
              case 'C-':
                sub_22 = 1.67;
                break;
              case 'D+':
                sub_22 = 1.33;
                break;
              case 'D':
                sub_22 = 1;
                break;
              case 'D-':
                sub_22 = .67;
                break;
              case 'E':
                sub_22 = 0;
                break;
            }

        }else{
          switch(grade_22){
              case 'C-':
                sub_22 = 1.67;
                break;
              case 'D+':
                sub_22 = 1.33;
                break;
              case 'D':
                sub_22 = 1;
                break;
              case 'D-':
                sub_22 = .67;
                break;
              case 'E':
                sub_22 = 0;
                break;
              default :
                sub_22 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_24')[0].checked){

            switch(grade_24){
              case 'Not Sat':
                sub_24 = 0;
                break;
              case 'A+':
                sub_24 = 4;
                break;
              case 'A':
                sub_24 = 4;
                break;
              case 'A-':
                sub_24 = 3.67;
                break;
              case 'B+':
                sub_24 = 3.33;
                break;
              case 'B':
                sub_24 = 3;
                break;
              case 'B-':
                sub_24 = 2.67;
                break;
              case 'C+':
                sub_24 = 2.33;
                break;
              case 'C':
                sub_24 = 2;
                break;
              case 'C-':
                sub_24 = 1.67;
                break;
              case 'D+':
                sub_24 = 1.33;
                break;
              case 'D':
                sub_24 = 1;
                break;
              case 'D-':
                sub_24 = .67;
                break;
              case 'E':
                sub_24 = 0;
                break;
            }

        }else{
          switch(grade_24){
              case 'C-':
                sub_24 = 1.67;
                break;
              case 'D+':
                sub_24 = 1.33;
                break;
              case 'D':
                sub_24 = 1;
                break;
              case 'D-':
                sub_24 = .67;
                break;
              case 'E':
                sub_24 = 0;
                break;
              default :
                sub_24 = 2;
            }
          
        }

        if(document.getElementsByName('sitting_sub_25')[0].checked){
          
            switch(grade_25){
              case 'Not Sat':
                sub_25 = 0;
                break;
              case 'A+':
                sub_25 = 4;
                break;
              case 'A':
                sub_25 = 4;
                break;
              case 'A-':
                sub_25 = 3.67;
                break;
              case 'B+':
                sub_25 = 3.33;
                break;
              case 'B':
                sub_25 = 3;
                break;
              case 'B-':
                sub_25 = 2.67;
                break;
              case 'C+':
                sub_25 = 2.33;
                break;
              case 'C':
                sub_25 = 2;
                break;
              case 'C-':
                sub_25 = 1.67;
                break;
              case 'D+':
                sub_25 = 1.33;
                break;
              case 'D':
                sub_25 = 1;
                break;
              case 'D-':
                sub_25 = .67;
                break;
              case 'E':
                sub_25 = 0;
                break;
            }

        }else{
          switch(grade_25){
              case 'C-':
                sub_25 = 1.67;
                break;
              case 'D+':
                sub_25 = 1.33;
                break;
              case 'D':
                sub_25 = 1;
                break;
              case 'D-':
                sub_25 = .67;
                break;
              case 'E':
                sub_25 = 0;
                break;
              default :
                sub_25 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_26')[0].checked){
          
            switch(grade_26){
              case 'Not Sat':
                sub_26 = 0;
                break;
              case 'A+':
                sub_26 = 4;
                break;
              case 'A':
                sub_26 = 4;
                break;
              case 'A-':
                sub_26 = 3.67;
                break;
              case 'B+':
                sub_26 = 3.33;
                break;
              case 'B':
                sub_26 = 3;
                break;
              case 'B-':
                sub_26 = 2.67;
                break;
              case 'C+':
                sub_26 = 2.33;
                break;
              case 'C':
                sub_26 = 2;
                break;
              case 'C-':
                sub_26 = 1.67;
                break;
              case 'D+':
                sub_26 = 1.33;
                break;
              case 'D':
                sub_26 = 1;
                break;
              case 'D-':
                sub_26 = .67;
                break;
              case 'E':
                sub_26 = 0;
                break;
            }

        }else{
          switch(grade_26){
              case 'C-':
                sub_26 = 1.67;
                break;
              case 'D+':
                sub_26 = 1.33;
                break;
              case 'D':
                sub_26 = 1;
                break;
              case 'D-':
                sub_26 = .67;
                break;
              case 'E':
                sub_26 = 0;
                break;
              default :
                sub_26 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_27')[0].checked){
          
            switch(grade_27){
              case 'Not Sat':
                sub_27 = 0;
                break;
              case 'A+':
                sub_27 = 4;
                break;
              case 'A':
                sub_27 = 4;
                break;
              case 'A-':
                sub_27 = 3.67;
                break;
              case 'B+':
                sub_27 = 3.33;
                break;
              case 'B':
                sub_27 = 3;
                break;
              case 'B-':
                sub_27 = 2.67;
                break;
              case 'C+':
                sub_27 = 2.33;
                break;
              case 'C':
                sub_27 = 2;
                break;
              case 'C-':
                sub_27 = 1.67;
                break;
              case 'D+':
                sub_27 = 1.33;
                break;
              case 'D':
                sub_27 = 1;
                break;
              case 'D-':
                sub_27 = .67;
                break;
              case 'E':
                sub_27 = 0;
                break;
            }

        }else{
          switch(grade_27){
              case 'C-':
                sub_27 = 1.67;
                break;
              case 'D+':
                sub_27 = 1.33;
                break;
              case 'D':
                sub_27 = 1;
                break;
              case 'D-':
                sub_27 = .67;
                break;
              case 'E':
                sub_27 = 0;
                break;
              default :
                sub_27= 2;
            }
        }

        var gpa = ((sub_1*3)+(sub_2*3)+(sub_3*3)+(sub_7*3)+(sub_8*4)+(sub_9*4)+(sub_10*3)+(sub_11*3)+(sub_12*3)+(sub_13*3)+(sub_14*3)+(sub_15*4)+(sub_16*4)+(sub_17*3)+(sub_18*4)+(sub_19*3)+(sub_20*3)+(sub_21*3)+(sub_22*3)+(sub_24*8)+(sub_25*3)+(sub_26*3)+(sub_27*3))/79;
        document.getElementById("full_degree_gpa").innerHTML = gpa.toFixed(2);

        var gpa_percentage = (gpa/4)*100;
        document.getElementById("gpa_percentage").innerHTML = gpa_percentage.toFixed(2)+"%";

        document.getElementById("heading").innerHTML = "Result Sheet";

        document.getElementById("subject_1").innerHTML = "1. Information Systems & Technology";
        document.getElementById("subject_2").innerHTML = "2. Computer Systems I";
        document.getElementById("subject_3").innerHTML = "3. Web Application Development I";
        document.getElementById("subject_4").innerHTML = "4. Communication Skills";
        document.getElementById("subject_5").innerHTML = "5. Introductory Mathematics";
        document.getElementById("subject_6").innerHTML = "6. Personal Computing";
        document.getElementById("subject_7").innerHTML = "7. Mathematics for Computing I";
        document.getElementById("subject_8").innerHTML = "8. Programming I";
        document.getElementById("subject_9").innerHTML = "9. Database Systems I";
        document.getElementById("subject_10").innerHTML = "10. Systems Analysis & Design";
        document.getElementById("subject_11").innerHTML = "11. Object Oriented Analysis & Design";
        document.getElementById("subject_12").innerHTML = "12. Fundamentals of Software Engineering";
        document.getElementById("subject_13").innerHTML = "13. Mathematics for Computing II";
        document.getElementById("subject_14").innerHTML = "14. User Interface Design";
        document.getElementById("subject_15").innerHTML = "15. Web Application Development II";
        document.getElementById("subject_16").innerHTML = "16. Programming II";
        document.getElementById("subject_17").innerHTML = "17. Information Technology Project Mgt.";
        document.getElementById("subject_18").innerHTML = "18. Rapid Software Development";
        document.getElementById("subject_19").innerHTML = "19. Computer Networks";
        document.getElementById("subject_20").innerHTML = "20. Professional Issues in IT";
        document.getElementById("subject_21").innerHTML = "21. Information Systems Security";
        document.getElementById("subject_22").innerHTML = "22. Fundamentals of Multimedia";
        document.getElementById("subject_23").innerHTML = "23. Fundamentals of Management";
        document.getElementById("subject_24").innerHTML = "24. Software Development Project";
        document.getElementById("subject_25").innerHTML = "25. Systems & Network Administration";
        document.getElementById("subject_26").innerHTML = "26. e-Business Application";
        document.getElementById("subject_27").innerHTML = "27. Database Systems II";
        document.getElementById("subject_28").innerHTML = "28. Introduction to Entrepreneurship";

        document.getElementById("type_1").innerHTML = "GPA";
        document.getElementById("type_2").innerHTML = "GPA";
        document.getElementById("type_3").innerHTML = "GPA";
        document.getElementById("type_4").innerHTML = "Non-GPA";
        document.getElementById("type_5").innerHTML = "Non-GPA";
        document.getElementById("type_6").innerHTML = "Non-GPA";
        document.getElementById("type_7").innerHTML = "GPA";
        document.getElementById("type_8").innerHTML = "GPA";
        document.getElementById("type_9").innerHTML = "GPA";
        document.getElementById("type_10").innerHTML = "GPA";
        document.getElementById("type_11").innerHTML = "GPA";
        document.getElementById("type_12").innerHTML = "GPA";
        document.getElementById("type_13").innerHTML = "GPA";
        document.getElementById("type_14").innerHTML = "GPA";
        document.getElementById("type_15").innerHTML = "GPA";
        document.getElementById("type_16").innerHTML = "GPA";
        document.getElementById("type_17").innerHTML = "GPA";
        document.getElementById("type_18").innerHTML = "GPA";
        document.getElementById("type_19").innerHTML = "GPA";
        document.getElementById("type_20").innerHTML = "GPA";
        document.getElementById("type_21").innerHTML = "GPA";
        document.getElementById("type_22").innerHTML = "GPA";
        document.getElementById("type_23").innerHTML = "Non-GPA";
        document.getElementById("type_24").innerHTML = "GPA";
        document.getElementById("type_25").innerHTML = "GPA";
        document.getElementById("type_26").innerHTML = "GPA";
        document.getElementById("type_27").innerHTML = "GPA";
        document.getElementById("type_28").innerHTML = "Non-GPA";

        document.getElementById("credit_1").innerHTML = "3";
        document.getElementById("credit_2").innerHTML = "3";
        document.getElementById("credit_3").innerHTML = "3";
        document.getElementById("credit_4").innerHTML = "2";
        document.getElementById("credit_5").innerHTML = "2";
        document.getElementById("credit_6").innerHTML = "2";
        document.getElementById("credit_7").innerHTML = "3";
        document.getElementById("credit_8").innerHTML = "4";
        document.getElementById("credit_9").innerHTML = "4";
        document.getElementById("credit_10").innerHTML = "3";
        document.getElementById("credit_11").innerHTML = "3";
        document.getElementById("credit_12").innerHTML = "3";
        document.getElementById("credit_13").innerHTML = "3";
        document.getElementById("credit_14").innerHTML = "3";
        document.getElementById("credit_15").innerHTML = "4";
        document.getElementById("credit_16").innerHTML = "4";
        document.getElementById("credit_17").innerHTML = "3";
        document.getElementById("credit_18").innerHTML = "4";
        document.getElementById("credit_19").innerHTML = "3";
        document.getElementById("credit_20").innerHTML = "3";
        document.getElementById("credit_21").innerHTML = "3";
        document.getElementById("credit_22").innerHTML = "3";
        document.getElementById("credit_23").innerHTML = "2";
        document.getElementById("credit_24").innerHTML = "8";
        document.getElementById("credit_25").innerHTML = "3";
        document.getElementById("credit_26").innerHTML = "3";
        document.getElementById("credit_27").innerHTML = "3";
        document.getElementById("credit_28").innerHTML = "2";

        document.getElementById("grade_1").innerHTML = grade_1;
        document.getElementById("grade_2").innerHTML = grade_2;
        document.getElementById("grade_3").innerHTML = grade_3;
        document.getElementById("grade_4").innerHTML = grade_4;
        document.getElementById("grade_5").innerHTML = grade_5;
        document.getElementById("grade_6").innerHTML = grade_6;
        document.getElementById("grade_7").innerHTML = grade_7;
        document.getElementById("grade_8").innerHTML = grade_8;
        document.getElementById("grade_9").innerHTML = grade_9;
        document.getElementById("grade_10").innerHTML = grade_10;
        document.getElementById("grade_11").innerHTML = grade_11;
        document.getElementById("grade_12").innerHTML = grade_12;
        document.getElementById("grade_13").innerHTML = grade_13;
        document.getElementById("grade_14").innerHTML = grade_14;
        document.getElementById("grade_15").innerHTML = grade_15;
        document.getElementById("grade_16").innerHTML = grade_16;
        document.getElementById("grade_17").innerHTML = grade_17;
        document.getElementById("grade_18").innerHTML = grade_18;
        document.getElementById("grade_19").innerHTML = grade_19;
        document.getElementById("grade_20").innerHTML = grade_20;
        document.getElementById("grade_21").innerHTML = grade_21;
        document.getElementById("grade_22").innerHTML = grade_22;
        document.getElementById("grade_23").innerHTML = grade_23;
        document.getElementById("grade_24").innerHTML = grade_24;
        document.getElementById("grade_25").innerHTML = grade_25;
        document.getElementById("grade_26").innerHTML = grade_26;
        document.getElementById("grade_27").innerHTML = grade_27;
        document.getElementById("grade_28").innerHTML = grade_28;

        document.getElementById("type").innerHTML = "Type";
        document.getElementById("subject").innerHTML = "Subject";
        document.getElementById("credit").innerHTML = "Credit";
        document.getElementById("grade").innerHTML = "Grade";

        document.getElementById("class_gpa").innerHTML = "Class GPA";
        document.getElementById("gpa_pect").innerHTML = "% of GPA";

        document.getElementById("table").style.border ="1px solid lightgray";
        document.getElementById("full_degree_gpa").style.background ="lightgreen";
        document.getElementById("gpa_percentage").style.background ="yellow";
        document.getElementById("table").style.background ="#fff";
        document.getElementById("result_topic").style.background ="#A8A7A7";

      }
    </script>
    
<?php 
include "inc/footer.php";
?>