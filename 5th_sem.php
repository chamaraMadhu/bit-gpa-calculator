<?php 
include "inc/header.php";
include "inc/navbar.php";
?>

    <div class="container mt-5 bg-light" style="min-height: 700px">
      <div class="row pt-2 pb-1" style="background-color: #FF847C ">
        <div class="col text-center ">
        <h5 class="text-center text-light">5<sup>th</sup> Semester GPA</h5></div>
      </div>
      <div class="row bg-white p-3" style="border-radius: 2px">
        <div class="col-sm-8 col-md-8">

          <div class="row p-3">
            <h5 class="text-muted mb-3">5<sup>th</sup> Semester</h5>
            <table>
                <tr>
                    <th width="350px">Subject</th>
                    <th class="text-center" width="100px">Type</th>
                    <th class="text-center" width="160px">Is 1st Sitting?</th>
                    <th class="text-center" width="100px">Grade</th>
                </tr>
                <tr>
                    <td>20. Professional Issues in IT</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_20" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_20" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_20">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>21. Information Systems Security</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_21" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_21" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_21">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>22. Fundamentals of Multimedia</td>
                    <td>GPA</td>
                    <td style="font-weight: 600">
                      <input type="radio" name="sitting_sub_22" value="yes" checked> <span class="text-success" >Yes</span> &nbsp; <input type="radio" name="sitting_sub_22" value="no"> <span class="text-danger">No</span>
                    </td>
                    <td>
                        <select id="sub_22">
                          <option>Not Sat</option>
                          <option>A+</option>
                          <option>A</option>
                          <option>A-</option>
                          <option>B+</option>
                          <option>B</option>
                          <option>B-</option>
                          <option>C+</option>
                          <option>C</option>
                          <option>C-</option>
                          <option>D+</option>
                          <option>D</option>
                          <option>D-</option>
                          <option>E</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>23. Fundamentals of Management</td>
                    <td>Nn-GPA</td>
                    <td></td>
                    <td>
                      <select id="sub_23" style="width:88px">
                          <option>Pass</option>
                          <option>Fail</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><button class="btn btn-success btn-sm mt-2" onClick="gpaCal_5();" style="width:100%">Calculate</button></td>
                </tr>
            </table>
          </div>

          <div class="row text-center" id="result_topic" >
            <div class="col">
            <h6 class="text-light pt-2 pb-1" id="heading"></h6>
            </div>
          </div>
          <div class="row pb-3" id="table">
            <div class="col">
              <table class="mt-3" align="center">
                <tr style="font-weight: 700">
                  <td id="subject" width="350"></td>
                  <td id="type" width="100"></td>
                  <td id="credit" width="160"></td>
                  <td id="grade" width="100"></td>
                </tr>
                <tr>
                  <td id="subject_20"></td>
                  <td id="type_20"></td>
                  <td id="credit_20"></td>
                  <td id="grade_20"></td>
                </tr>
                <tr>
                  <td id="subject_21"></td>
                  <td id="type_21"></td>
                  <td id="credit_21"></td>
                  <td id="grade_21"></td>
                </tr>
                <tr>
                  <td id="subject_22"></td>
                  <td id="type_22"></td>
                  <td id="credit_22"></td>
                  <td id="grade_22"></td>
                </tr>
                <tr>
                  <td id="subject_23"></td>
                  <td id="type_23"></td>
                  <td id="credit_23"></td>
                  <td id="grade_23"></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td id="class_gpa" style="font-size: 14px; font-weight: 500"></td>
                  <td id="sem_5_gpa" style="font-size: 14px; font-weight: 500"></td>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                  <td id="gpa_pect" style="font-size: 14px; font-weight: 500"></td>
                  <td id="gpa_percentage" style="font-size: 14px; font-weight: 500"></td>
                </tr>
              </table>
            </div>
          </div>

        </div>

        <div class="col-sm-4 col-md-4 Grading_scheme p-3">
          <h5 class="text-muted text-center mb-3">Grading Scheme</h5>
          <table align="center" border="1">
              <tr>
                  <th class="text-center" width="75px">Marks</th>
                  <th class="text-center" width="80px">Grade</th>
                  <th class="text-center" width="100px">Point Value</th>
              </tr>
              <tr>
                <td>90-100</td>
                <td>A+</td>
                <td>4.00</td>
              </tr>
              <tr>
                <td>80-89</td>
                <td>A</td>
                <td>4.00</td>
              </tr>
              <tr>
                <td>75-79</td>
                <td>A-</td>
                <td>3.67</td>
              </tr>
              <tr>
                <td>70-74</td>
                <td>B+</td>
                <td>3.33</td>
              </tr>
              <tr>
                <td>65-69</td>
                <td>B</td>
                <td>3.00</td>
              </tr>
              <tr>
                <td>60-64</td>
                <td>B-</td>
                <td>2.67</td>
              </tr>
              <tr>
                <td>55-59</td>
                <td>C+</td>
                <td>2.33</td>
              </tr>
              <tr>
                <td>50-54</td>
                <td>C</td>
                <td>2.00</td>
              </tr>
              <tr>
                <td>45-49</td>
                <td>C-</td>
                <td>1.67</td>
              </tr>
              <tr>
                <td>40-44</td>
                <td>D+</td>
                <td>1.33</td>
              </tr>
              <tr>
                <td>30-39</td>
                <td>D</td>
                <td>1.00</td>
              </tr>
              <tr>
                <td>20-29</td>
                <td>D-</td>
                <td>0.67</td>
              </tr>
              <tr>
                <td>0-19</td>
                <td>E</td>
                <td>0.00</td>
              </tr>
              <tr>
                <td>-</td>
                <td style="padding-left: 0px; text-align: center">Not Sat</td>
                <td>0.00</td>
              </tr>
          </table>
          <div style="font-size: 14px">
            <p class="text-center mt-3"><b class="text-danger">**</b> Not Sat = You don't Still face the subject or skipped</p>
            <p class="text-center mt-0"><b class="text-danger">**</b> Non-GPA = Not Applicable for GPA calculation</p>
          </div>
        </div>

      </div>
    </div>

     <script type="text/javascript">
      function gpaCal_5(){

        var grade_20 = document.getElementById('sub_20').value;
        var grade_21 = document.getElementById('sub_21').value;
        var grade_22 = document.getElementById('sub_22').value;
        var grade_23 = document.getElementById('sub_23').value;

        var sub_20;
        var sub_21;
        var sub_22;
        var sub_23;

        if(document.getElementsByName('sitting_sub_20')[0].checked){

            switch(grade_20){
              case 'Not Sat':
                sub_20 = 0;
                break;
              case 'A+':
                sub_20 = 4;
                break;
              case 'A':
                sub_20 = 4;
                break;
              case 'A-':
                sub_20 = 3.67;
                break;
              case 'B+':
                sub_20 = 3.33;
                break;
              case 'B':
                sub_20 = 3;
                break;
              case 'B-':
                sub_20 = 2.67;
                break;
              case 'C+':
                sub_20 = 2.33;
                break;
              case 'C':
                sub_20 = 2;
                break;
              case 'C-':
                sub_20 = 1.67;
                break;
              case 'D+':
                sub_20 = 1.33;
                break;
              case 'D':
                sub_20 = 1;
                break;
              case 'D-':
                sub_20 = .67;
                break;
              case 'E':
                sub_20 = 0;
                break;
            }

        }else{
          switch(grade_20){
              case 'C-':
                sub_20 = 1.67;
                break;
              case 'D+':
                sub_20 = 1.33;
                break;
              case 'D':
                sub_20 = 1;
                break;
              case 'D-':
                sub_20 = .67;
                break;
              case 'E':
                sub_20 = 0;
                break;
              default :
                sub_20 = 2;
            }
          
        }

        if(document.getElementsByName('sitting_sub_21')[0].checked){
          
            switch(grade_21){
              case 'Not Sat':
                sub_21 = 0;
                break;
              case 'A+':
                sub_21 = 4;
                break;
              case 'A':
                sub_21 = 4;
                break;
              case 'A-':
                sub_21 = 3.67;
                break;
              case 'B+':
                sub_21 = 3.33;
                break;
              case 'B':
                sub_21 = 3;
                break;
              case 'B-':
                sub_21 = 2.67;
                break;
              case 'C+':
                sub_21 = 2.33;
                break;
              case 'C':
                sub_21 = 2;
                break;
              case 'C-':
                sub_21 = 1.67;
                break;
              case 'D+':
                sub_21 = 1.33;
                break;
              case 'D':
                sub_21 = 1;
                break;
              case 'D-':
                sub_21 = .67;
                break;
              case 'E':
                sub_21 = 0;
                break;
            }

        }else{
          switch(grade_21){
              case 'C-':
                sub_21 = 1.67;
                break;
              case 'D+':
                sub_21 = 1.33;
                break;
              case 'D':
                sub_21 = 1;
                break;
              case 'D-':
                sub_21 = .67;
                break;
              case 'E':
                sub_21 = 0;
                break;
              default :
                sub_21 = 2;
            }
        }

        if(document.getElementsByName('sitting_sub_22')[0].checked){
          
            switch(grade_22){
              case 'Not Sat':
                sub_22 = 0;
                break;
              case 'A+':
                sub_22 = 4;
                break;
              case 'A':
                sub_22 = 4;
                break;
              case 'A-':
                sub_22 = 3.67;
                break;
              case 'B+':
                sub_22 = 3.33;
                break;
              case 'B':
                sub_22 = 3;
                break;
              case 'B-':
                sub_22 = 2.67;
                break;
              case 'C+':
                sub_22 = 2.33;
                break;
              case 'C':
                sub_22 = 2;
                break;
              case 'C-':
                sub_22 = 1.67;
                break;
              case 'D+':
                sub_22 = 1.33;
                break;
              case 'D':
                sub_22 = 1;
                break;
              case 'D-':
                sub_22 = .67;
                break;
              case 'E':
                sub_22 = 0;
                break;
            }

        }else{
          switch(grade_22){
              case 'C-':
                sub_22 = 1.67;
                break;
              case 'D+':
                sub_22 = 1.33;
                break;
              case 'D':
                sub_22 = 1;
                break;
              case 'D-':
                sub_22 = .67;
                break;
              case 'E':
                sub_22 = 0;
                break;
              default :
                sub_22 = 2;
            }
        }

        var gpa = ((sub_20*3)+(sub_21*3)+(sub_22*3))/9;
        document.getElementById("sem_5_gpa").innerHTML = gpa.toFixed(2);

        var gpa_percentage = (gpa/4)*100;
        document.getElementById("gpa_percentage").innerHTML = gpa_percentage.toFixed(2)+"%";

        document.getElementById("heading").innerHTML = "Result Sheet";
        document.getElementById("subject_20").innerHTML = "20. Professional Issues in IT";
        document.getElementById("subject_21").innerHTML = "21. Information Systems Security";
        document.getElementById("subject_22").innerHTML = "22. Fundamentals of Multimedia";
        document.getElementById("subject_23").innerHTML = "23. Fundamentals of Management";

        document.getElementById("type_20").innerHTML = "GPA";
        document.getElementById("type_21").innerHTML = "GPA";
        document.getElementById("type_22").innerHTML = "GPA";
        document.getElementById("type_23").innerHTML = "Non-GPA";

        document.getElementById("credit_20").innerHTML = "3";
        document.getElementById("credit_21").innerHTML = "3";
        document.getElementById("credit_22").innerHTML = "3";
        document.getElementById("credit_23").innerHTML = "2";

        document.getElementById("grade_20").innerHTML = grade_20;
        document.getElementById("grade_21").innerHTML = grade_21;
        document.getElementById("grade_22").innerHTML = grade_22;
        document.getElementById("grade_23").innerHTML = grade_23;

        document.getElementById("type").innerHTML = "Type";
        document.getElementById("subject").innerHTML = "Subject";
        document.getElementById("credit").innerHTML = "Credit";
        document.getElementById("grade").innerHTML = "Grade";

        document.getElementById("class_gpa").innerHTML = "Class GPA";
        document.getElementById("gpa_pect").innerHTML = "% of GPA";

        document.getElementById("table").style.border ="1px solid lightgray";
        document.getElementById("sem_5_gpa").style.background ="lightgreen";
        document.getElementById("gpa_percentage").style.background ="yellow";
        document.getElementById("table").style.background ="#fff";
        document.getElementById("result_topic").style.background ="#A8A7A7";

      }
    </script>
    
<?php 
include "inc/footer.php";
?>